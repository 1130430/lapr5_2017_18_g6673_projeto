import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { Farmaco } from '../../models/Farmaco';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Prescricao } from '../../models/Prescricao';
import { Apresentacao } from '../../models/Apresentacao';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import * as moment from 'moment';
import { Medicamento } from '../../models/Medicamento';
import { Posologia } from '../../models/Posologia';

@Component({
    selector: 'app-modal-prescription',
    templateUrl: './prescription.component.html',
    styleUrls: ['./prescription.component.scss']
})
export class PrescriptionComponent implements OnInit {
    farmacos      : Array<Farmaco> = [];
    apresentacoes : Array<Apresentacao> = [];
    medicamentos  : Array<Medicamento> = [];
    minDate       : moment.Moment = moment();
    bsConfig      : Partial<BsDatepickerConfig>;
    form          : Partial<Prescricao> = {
        idFarmaco      : 0,
        idApresentacao : 0,
        idMedicamento  : 0,
        posologia      : '',
        quantidade     : 1,
        dataValidade   : null,
        _id            : ""
    };
    posologias = "";
    canceled : boolean = false;
    constructor(public bsModalRef: BsModalRef, private http: HttpClient, private toastr: ToastrService) { 
        this.bsConfig = {showWeekNumbers: false, dateInputFormat: "DD/MM/YYYY"};
    }
    
    ngOnInit() {
        this.canceled = false;
    }

    selectChanged(affectedProperty: string) {
        this.form[affectedProperty] = 0;
    }

    updatePosologias() {
        if (this.form.idMedicamento == 0) { // The user chose no medicine
            this.posologias = "";
            return;
        }
        this.http.get<Posologia[]>(`medicamentos/${this.form.idMedicamento}/posologias`).subscribe(
            response => {
                this.posologias = response.map((posologia) => {
                    return `Faixa etária: ${posologia.faixaEtaria} -> ${posologia.frequencia}`;
                }).join("<br>");

                this.posologias = `<b>Recomendações</b><br><br>${this.posologias}`;
            },
            err => this.posologias = ""
        );
    }

    create() {
        this.bsModalRef.hide();
    }

    isInvalid(field: any): boolean {
        return field.invalid && (field.dirty || field.touched);
    }

    cancel() {
        this.canceled = true;
        this.bsModalRef.hide();
    }
    
    private handleError(err: any) {
        this.toastr.error(err.error.message, 'Erro');
    }
    
}
