import { Injectable } from '@angular/core';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { User } from '../models/User';

@Injectable()
export class AuthService {
    
    farmaciaNome = '';
    tokenName = 'token';
    constructor() { }
    
    public getToken(): string {
        return localStorage.getItem(this.tokenName);
    }

    public getFarmacia(): string {
        return localStorage.getItem(this.farmaciaNome);
    }

    public setFarmacia(farmacia: string): void {
        localStorage.setItem(this.farmaciaNome, farmacia);
    }

    public setToken(token: string): void {
        localStorage.setItem(this.tokenName, token);
    }

    public removeToken(): void {
        localStorage.removeItem(this.tokenName);
    }

    public removeFarmacia(): void {
        localStorage.removeItem(this.farmaciaNome);
    }
    
    public isAuthenticated(): boolean {
        // return a boolean reflecting 
        // whether or not the token is expired
        return tokenNotExpired(this.tokenName);
    }

    public getUser(): User {
        let token = this.isAuthenticated() && this.getToken();
        
        if (!token) {
            return {id: "---", name: "Convidado", email: "", role: "guest"};
        }

        return new JwtHelper().decodeToken(token);
    }

    public hasRole(role: string[] | string): boolean {
        if (!this.isAuthenticated()) {
            return false;
        }

        let userRole = this.getUser().role;

        // Make sure the role variable is always an array
        if (typeof role === 'string') {
            role = [role];
        }
        // console.log(role, userRole);
        return role.indexOf(userRole) !== -1;
    }
}
