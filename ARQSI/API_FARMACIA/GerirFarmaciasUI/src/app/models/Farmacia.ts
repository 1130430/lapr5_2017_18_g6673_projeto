import { Stock } from "./Stock";

export class Farmacia {
    id           : string;
    _id          : string;
    name         : string;
    stocks       : Stock[];
}