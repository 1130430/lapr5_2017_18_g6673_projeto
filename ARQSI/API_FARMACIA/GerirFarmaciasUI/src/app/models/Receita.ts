import { User } from "./User";
import { Prescricao } from "./Prescricao";

export class Receita {
    _id         : string;
    medico      : User;
    utente      : User;
    prescricoes : Prescricao[];
    createdAt   : Date;
    updatedAt   : Date;
}
