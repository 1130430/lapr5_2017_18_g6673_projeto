import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Apresentacao } from '../../models/Apresentacao';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { PresentationsCommentComponent } from '../../dialogs/presentations-comment/presentations-comment.component';


@Component({
    selector: 'app-presentations',
    templateUrl: 'presentations.component.html'
})
export class PresentationsComponent implements OnInit { 
    bsModalRef    : BsModalRef;
    apresentacoes : Array<Apresentacao> = [];
    search        : string = "";
    loading       : boolean = false;
    constructor(
        private http: HttpClient, 
        private toastr: ToastrService,
        private modalService: BsModalService
    ) {}
    
    ngOnInit() {
        this.getPresentations();
    }
    
    getPresentations() {
        this.loading = true;
        this.http.get<Apresentacao[]>('apresentacoes')
        .subscribe(
            response => {
                this.loading = false;
                this.apresentacoes = response;
            },
            err => this.handleError(err)
        );
    }

    showCommentModal(id: number) {
        this.bsModalRef = this.modalService.show(PresentationsCommentComponent, {class: 'modal-lg'});
        this.bsModalRef.content.id = id;
        this.bsModalRef.content.loadComments();
    }

    private handleError(err) {
        if (this.loading) {
            this.loading = false;
        }
        this.toastr.error(err.error.message, 'Erro');
    }
}
