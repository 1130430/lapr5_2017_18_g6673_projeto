import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../services/auth.service';
import { Receita } from '../../models/index';
import { Prescricao } from '../../models/Prescricao';
import { Farmacia } from '../../models/Farmacia';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    public user = { email: '', name: '' };
    public med = { medicamento: '', quantidade: 0 };
    farmacias: Array<Farmacia> = [];
    model: any = {};

    public aviamentos = 0;
    pies = {
        prescricoes: {
            labels: ["Por aviar", "Parcialmente aviadas", "Completamente aviadas"],
            data: [0, 0, 0],
            loading: false
        },

        stocks: {
            labels: [],
            data: [],

            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    enabled: false
                },
                scales: {
                    xAxes: [{
                        ticks:
                        {
                            autoSkip: false
                        }
                    }],
                    yAxes: [{
                            ticks: {
                                suggestedMin: 0,
                                sggestedmax: 100,
                            },
                        }]
                }
            },
            loading: false
        }

        
    };
    constructor(
        private http: HttpClient,
        public authService: AuthService,
        private toastr: ToastrService
    ) { }

    ngOnInit() {
        this.loadData();

    }

    loadData() {
        this.loadPrescriptions();
        this.loadFarmacias();
        //this.loadStocks();
        this.user = this.authService.getUser();
        this.loadAviamentos();
        this.maisAviado();
    }

    private loadPrescriptions() {
        this.pies.prescricoes.loading = true;
        this.http.get<any>('chart/prescricoes').subscribe(
            response => {
                let data = [response.por_aviar, response.parcial, response.aviado];
                // Need to replace the original array otherwise the chart wont update
                this.pies.prescricoes.data = data;
                this.pies.prescricoes.loading = false;
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
                this.pies.prescricoes.loading = false;
            }
        );
    }

    private loadAviamentos() {
        this.http.get<any>('chart/prescricoesFarmaceutico', {
            params: {
                farmaceutico: this.authService.getUser().id
            }
        }).subscribe(
            response => {
                console.log(response);
                this.aviamentos = response
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
            }
            );
    }

    private maisAviado() {
        this.http.get<any>('chart/medicamentos/maisaviado').subscribe(
            response => {
                this.med = response;
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
            }
        );
    }

    private loadFarmacias() {
        this.http.get<Farmacia[]>('farmacias').subscribe(
            res => {
                this.farmacias = res;
                console.log(this.farmacias);
            }, err => {
                this.toastr.error(err.error.message, 'Erro');
            });
    }

    public loadStocks() {
        //var f = this.authService.getFarmacia();
        this.pies.stocks.data = [];
        this.pies.stocks.labels = [];
        this.pies.stocks.loading = true;
        this.http.get<any>('chart/getStock/', {
            params: {
                farmaciaName: this.model.farmaciaName
            }
        }).subscribe(
            stocks => {

                stocks.forEach(s => {

                    this.pies.stocks.data.push(s.quantidade);
                    this.pies.stocks.labels.push(s.medicamento);
                });
                console.log(this.pies.stocks.labels);
                this.pies.stocks.loading = false;
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
                this.pies.stocks.loading = false;
            }
            );
    }
}
