import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { DashboardComponent } from './dashboard.component';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [ DashboardRoutingModule, SharedModule, ChartsModule ],
    declarations: [
        DashboardComponent
    ]
})
export class DashboardModule { }
