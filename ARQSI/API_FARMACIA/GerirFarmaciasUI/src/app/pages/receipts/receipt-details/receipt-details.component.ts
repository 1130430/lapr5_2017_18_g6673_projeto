import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Receita, Prescricao } from '../../../models/index';
import { DispatchPrescriptionComponent } from '../../../dialogs/dispatch-prescription/dispatch-prescription.component';
import { Subscription } from 'rxjs/Subscription';
import * as moment from 'moment';
import { AuthService } from '../../../services/auth.service';

@Component({
    selector: 'app-receipt-details',
    templateUrl: './receipt-details.component.html',
    styleUrls: ['./receipt-details.component.scss']
})
export class ReceiptDetailsComponent implements OnInit, OnDestroy {
    
    id             : string;
    receita        : Receita = new Receita();
    loading        : boolean = false;
    sub            : Subscription;              // The route subscription object to handle params received in the url
    modalSub       : Subscription;              // The modal subscription object that handles when the modal is dismissed
    bsModalRef     : BsModalRef;
    currentEditing : number = -1;
    constructor(
        private http: HttpClient, 
        public authService: AuthService,
        private activeRoute: ActivatedRoute, 
        private toastr: ToastrService,
        private router: Router,
        private modalService: BsModalService
    ) { }
    
    ngOnInit() : void {
        this.createHandler();
        
        this.sub = this.activeRoute.params.subscribe(params => {
            this.id = params['id'];
            
            this.populateForm();
        });
    }
    
    ngOnDestroy(): void {
        this.sub.unsubscribe();
        this.destroyHandler();
    }
    
    dispatch(number: number) {
        this.currentEditing = number;
        let p = this.receita.prescricoes[number];
        let ref = this.setupModal();

        ref.content.prescricao = p;

        this.bsModalRef = ref;
    }
    
    private setupModal() : BsModalRef {
        let ref = this.modalService.show(DispatchPrescriptionComponent, {class: 'modal-lg'});
        return ref;
    }
    
    private createHandler() {
        this.modalSub = this.modalService.onHide.subscribe(reason => {
            if (reason || this.bsModalRef.content.canceled) { // Backdrop click
                return;
            }
            
            this.loading = true;
            let pId = this.receita.prescricoes[this.currentEditing]._id;
            let form = this.bsModalRef.content.form;
            this.http.put<Prescricao>(`receitas/${this.id}/prescricoes/${pId}/aviar`, {quantidade: form.quantidade, 
                farmaciaName : this.authService.getFarmacia(), medicamento : this.receita.prescricoes[this.currentEditing].nomeMedicamento})
            .subscribe(
                p => {
                    this.receita.prescricoes[this.currentEditing] = p;
                    this.loading = false;
                    this.toastr.success('Prescrição aviada com sucesso', 'Sucesso');
                    
                    this.receita.updatedAt = moment().toDate();
                },
                err => this.handleError(err)
            );
        });
    }
    
    private sanitize(data) {
        // Remove fields that are not supposed to be sent to the api
        if (this.currentEditing === -1) {
            delete data._id;
        }
        
        return data;
    }
    
    private destroyHandler() {
        this.modalSub.unsubscribe();
    }
    
    private populateForm() {
        this.loading = true;
        this.http.get<Receita>(`receitas/${this.id}`)
        .subscribe(data => {
            this.receita = data;
            this.loading = false;
        }, err => this.handleError(err));
    }
    
    private handleError(err: HttpErrorResponse) {
        if (err.status === 404) {
            this.toastr.error("A receita especificada não existe.", "Erro");
            this.router.navigate(['/receitas']);
            return;
        }
        
        if (this.loading) {
            this.loading = false;
        }
        
        this.toastr.error(err.error.message, 'Erro');
    }
    
}
