import { NgModule } from '@angular/core';

import { ReceiptsRoutingModule } from './receipts-routing.module';

import { ReceiptsComponent } from './receipts.component';
import { ReceiptDetailsComponent } from './receipt-details/receipt-details.component';
import { SearchReceiptComponent } from './search-receipt/search-receipt.component';

import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [ ReceiptsRoutingModule, SharedModule ],
    declarations: [
        ReceiptDetailsComponent,
        ReceiptsComponent,
        SearchReceiptComponent
    ]
})
export class ReceiptsModule { }
