import { Component} from '@angular/core';
import { Receita } from '../../../models/index';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-search-receipt',
    templateUrl: './search-receipt.component.html',
    styleUrls: ['./search-receipt.component.scss']
})

export class SearchReceiptComponent {
    id       : string;
    receita  : Receita;
    searched : boolean = false;
    loading  : boolean = false;
    
    constructor(
        private http: HttpClient,
        private toastr: ToastrService,
        public authService: AuthService
    ) { }

    search() {
        this.loading = true;
        this.http.get<Receita>(`receitas/${this.id}`)
        .subscribe(
            response => {
                this.searched = true;
                this.loading = false;
                this.receita = response;
            },
            err => {
                this.searched = true;
                this.loading = false;
                this.receita = null;
            }
        );
    }
}
