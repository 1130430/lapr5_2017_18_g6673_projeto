export const environment = {
  production: true,
  apiUrl     : 'http://gerirfarmaciaapi.azurewebsites.net/api',
  socketUrl  : 'http://gerirfarmaciaapi.azurewebsites.net'
};
