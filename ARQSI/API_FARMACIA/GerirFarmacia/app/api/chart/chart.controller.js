const utils = require('../../components/utils');
const moment = require('moment');
const gdr = require('../../components/gestao-receitas');
const Farmacia = require('../../models/Farmacia');

function prescricoes(req, res) {
    gdr.getData("/api/chart/prescricoes", function (data) {
        console.log(data);
        res.status(200).json(data);
    });
}

function medicamentoMaisAviado(req, res) {
    gdr.getData("/api/chart/medicamentos/maisaviado", function (data) {
        console.log(data);
        res.status(200).json(data);
    });
}

function porAviar(req, res) {
    gdr.getData("/api/chart/prescricoes/poraviar", function (data) {
        console.log(data);
        res.status(200).json(data);
    });
}

function aviadasFarmaceutico(req, res) {
    gdr.getData("/api/chart/prescricoesFarmaceutico?farmaceutico="+req.query.farmaceutico, function (data) {
        console.log(data);
        res.status(200).json(data);
    });
}

function getStock(req, res) {
    Farmacia.findOne({ name: req.query.farmaciaName })
        .then(farmacia => {
            
            if (!farmacia) {
                return res.status(404).json({ error: 'not_found', message: 'A farmacia não existe' })
            }
            return res.status(200).json(farmacia.stocks);
        })
        .catch(utils.handleError(req, res));
}

module.exports = {
    prescricoes: prescricoes,
    porAviar: porAviar,
    getStock: getStock,
    aviadasFarmaceutico: aviadasFarmaceutico,
    medicamentoMaisAviado :medicamentoMaisAviado
};