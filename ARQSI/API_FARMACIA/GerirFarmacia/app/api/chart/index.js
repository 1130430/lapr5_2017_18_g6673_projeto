const router     = require('express').Router();
const controller = require('./chart.controller');
const auth       = require('../middleware/auth.middleware');

router.get('/prescricoes', controller.prescricoes);
router.get('/prescricoes/poraviar', auth.isAuthenticated(), controller.porAviar);
router.get('/getStock', controller.getStock);
router.get('/prescricoesFarmaceutico', controller.aviadasFarmaceutico);
router.get('/medicamentos/maisaviado',controller.medicamentoMaisAviado);
module.exports = router;