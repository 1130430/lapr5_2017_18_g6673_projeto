
const Farmacia      = require('../../models/Farmacia');
var async = require('async');
const utils     = require('../../components/utils');
const log4js = require('log4js');
log4js.configure({
    appenders: {
      app: { type: 'file', filename: './log/app.log', maxLogSize: 10485760, numBackups: 3 }
    },
    categories: {
      default: { appenders: ['app'], level: 'info' }
    }
  });

const logger = log4js.getLogger('info');

function show(req, res){
    logger.info("A mostrar todas as farmácias existentes...");
    Farmacia.find(function(err,farmacias){
        if(err) 
            res.send(err);
        res.json(farmacias);
    });
}

function create(req,res){
    let farmacia            = new Farmacia();
    let farmaceuticos       = req.body.farmaceuticos || [];
    let stocks              = req.body.stocks || [];
    logger.info("A criar uma nova farmácia com o nome " + req.body.name + " e id " + req.body.id);
    farmacia.name           = req.body.name;
    farmacia.id             = req.body.id;
    farmacia.farmaceuticos  = farmaceuticos;
    farmacia.stocks  = stocks;
    
    farmacia.save()
    .then(r => {
        return res.status(201).json(r);
    })
    .catch(utils.handleError(req, res));
}

function refillStock(req,res){
    console.log(req.body.farmacias);
    req.body.farmacias.forEach(s => {
        console.log(s);
        Farmacia.findOne({name : s})
        .then(farmacia => {
            if (!farmacia) {
                return res.status(404).json({ error: 'not_found', message: 'A farmacia não existe' })
            }
            farmacia.stocks.forEach(stock => {
                stock.quantidade = 100;
                farmacia.save();
            });
            res.status(200).json({ message: 'Stock Cheio!' });
        });
    });
}

module.exports = {
    show   : show,
    create : create,
    refillStock : refillStock
};