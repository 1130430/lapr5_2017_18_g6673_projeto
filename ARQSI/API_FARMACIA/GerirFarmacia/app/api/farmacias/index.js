const router     = require('express').Router();
const controller = require('./farmacias.controller');

router.get('/', controller.show);

router.post('/',controller.create);

router.put('/refillStock',controller.refillStock)

module.exports = router;