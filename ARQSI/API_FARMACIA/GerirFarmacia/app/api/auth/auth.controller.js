const User        = require('../../models/User');
const authService = require('./auth.service');
const utils       = require('../../components/utils');
const _           = require('lodash');
const gdr   = require('../../components/gestao-receitas');
const config = require('../../config/environment');
const Farmacia      = require('../../models/Farmacia');

function login(req, res){


    config.gestaoReceitas.email = req.body.email;
    config.gestaoReceitas.password = req.body.password;

        var token = gdr.getToken(function (data) {
            console.log(data.userID);
            
            if(data.role === "farmaceutico"){
                
                Farmacia.find({})
                .then(farmacias => {
                    farmacias.forEach(f => {
                        f.farmaceuticos.forEach(x => {
                            if (x.farmaceuticoID === data.userID) {
                                
                                res.status(200).json({token : data.token,expirationDate : data.expirationDate, farmaciaID : f._id, farmaciaNome : f.name});
                            }
                        });
                    
                    });
                })
                
            } else 
                res.status(401).json({error: 'invalid_user', message:"Utilizador Inválido!"});
        });
}


module.exports = {
    login : login
};