const router     = require('express').Router({mergeParams: true});
const controller = require('./prescricoes.controller');
const auth       = require('../../middleware/auth.middleware');

router.get('/:prescricaoId', controller.show);
router.put('/:prescricaoId/aviar', controller.aviar);

module.exports = router;