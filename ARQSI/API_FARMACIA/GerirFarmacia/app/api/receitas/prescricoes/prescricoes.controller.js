const moment = require('moment');
const utils = require('../../../components/utils');
const websocket = require('../../../components/websocket');
const gdr = require('../../../components/gestao-receitas');
const gdf = require('../../../components/gestao-fornecedor');
const Farmacia = require('../../../models/Farmacia');
const log4js = require('log4js');

log4js.configure({
    appenders: {
        app: { type: 'file', filename: 'log/app.log', maxLogSize: 10485760, numBackups: 3 }

    },
    categories: {
        default: { appenders: ['app'], level: 'info' }
    }
});

const logger = log4js.getLogger('info');

function show(req, res) {
    /*findPrescricao(req.params.receitaId, req.params.prescricaoId)
    .then(prescricao => res.status(200).json(prescricao))
    .catch(err => res.status(404).json(err));*/
}

function aviar(req, res) {
    console.log(req.body.farmaciaName);
    Farmacia.findOne({ name: req.body.farmaciaName })
        .then(farmacia => {
            if (!farmacia) {
                return res.status(404).json({ error: 'not_found', message: 'A farmacia não existe' })
            }
            farmacia.stocks.forEach(stock => {
                if (stock.medicamento === req.body.medicamento) {
                    console.log('receita '+req.params.receitaId);
                    if (stock.quantidade > 0) {
                        console.log('receita '+req.params.receitaId);
                        stock.quantidade -= req.body.quantidade;
                        farmacia.save();
                        console.log('receita '+req.params.receitaId);
                                logger.info("A aviar a receita com id " + req.params.receitaId + " para a prescrição com id " + req.params.prescricaoId + "...");
                                gdr.putData("/api/receitas/" + req.params.receitaId + "/prescricoes/" + req.params.prescricaoId + "/aviar", req.body, function (data) {
                                    console.log(stock.quantidade);
                                    
                                    if (stock.quantidade < stock.limite) {
                                        requestStock(req.body.farmaciaName, function (result) {
                                    
                                    res.status(200).json(data);
                                    });
                                    
                                }else{
                                    res.status(200).json(data);
                                }
                            });
                        
                    } else {
                        return res.status(404).json({ error: 'not_found', message: 'Medicamento não disponivel' })
                    }
                }
            });
        }).catch(utils.handleError(req, res));
}

function requestStock(req, callback) {
    var info = { nomeFarmacia: req }
    gdf.putData("/api/ordens", info, function (data) {
        return callback(data);
    });
}



module.exports = {
    show: show,
    aviar: aviar
};