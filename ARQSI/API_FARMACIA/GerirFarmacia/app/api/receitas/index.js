const router     = require('express').Router();
const controller = require('./receitas.controller');
const auth       = require('../middleware/auth.middleware');

router.get('/:id', controller.show);

router.use('/:receitaId/prescricoes', require('./prescricoes'));

module.exports = router;