const moment    = require('moment');
const utils     = require('../../components/utils');
const User      = require('../../models/User');
const mailer    = require('../../components/mailer');
const websocket = require('../../components/websocket');
const gdr   = require('../../components/gestao-receitas');
const log4js = require('log4js');
log4js.configure({
    appenders: {
      app: { type: 'file', filename: './log/app.log', maxLogSize: 10485760, numBackups: 3 }
    },
    categories: {
      default: { appenders: ['app'], level: 'info' }
    }
  });

const logger = log4js.getLogger('info');

function show(req, res){
    console.log(req.params.id);
    logger.info("A fazer pedido GET a Receitas com parâmetros: " + req.params.id);
    gdr.getData("/api/receitas/"+req.params.id,function (data) {
        console.log(data);
        res.status(200).json(data);
    });
}

module.exports = {
    show   : show
};