const _ = require('lodash');

let env = process.env.NODE_ENV || 'development';

// All configurations will extend these options
// ============================================
let all = {
    env: env,
    
    // Server port
    port: process.env.PORT || 9000,
    
    // Mongoose connection 
    mongoose: {
        uri: 'mongodb://lapr5:lapr5@ds139067.mlab.com:39067/farmacias'
        
    },

    jwt: {
        secret   : "fSk35bzq6KutR0dQVKTL",
        issuer   : "http://projeto.arqsi.local",
        audience : "Everyone"
    },

    gestaoReceitas: {
        url      : 'https://gestaoreceitaslapr5.azurewebsites.net',
        email    : 'email@example.com',
        password : 'password',
        token    : ''
    },

    fornecedor: {
        url      : 'http://localhost:8080'
    },

    mail: {
        host     : 'mail.smtp2go.com',
        port     : 2525,
        username : 'lapr5',
        password : 'NnZxbTJqYnU4d3Aw'
    }
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
    all,
    require(`./${env}.js`)
);