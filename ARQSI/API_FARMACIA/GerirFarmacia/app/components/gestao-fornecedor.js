var Client = require('node-rest-client').Client;
var client = new Client();
const config = require('../config/environment');



function getData(path, callback) {
    var args = {
        headers: { "Content-Type": "application/json"}
    };
    var url = config.fornecedor.url + path;
    client.get(url, args, function (data, response) {
        return callback(data);
    });

console.log("GetData");
}

function putData(path,info, callback) {

    console.log("PostData");

    var url = config.fornecedor.url + path;
    var args = {
        data: info,
        headers: { "Content-Type": "application/json"}
    };

    client.post(url, args, function (data, response) {
        console.log(data);
        return callback(data);
    });

  
}

exports.getData = getData;
exports.putData = putData;