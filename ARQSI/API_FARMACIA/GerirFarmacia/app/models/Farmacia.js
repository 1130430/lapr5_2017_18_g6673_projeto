const mongoose    = require('mongoose');
const Schema      = mongoose.Schema;

var FarmaciaSchema   = new Schema({
    name: String,
    id: Number,
    farmaceuticos: [{
        farmaceuticoID: String
    }],
    stocks: [{
        medicamento: String,
        quantidade: Number,
        limite: Number,
    }]
});

module.exports = mongoose.model('Farmacia', FarmaciaSchema);