const _ = require('lodash');

let env = process.env.NODE_ENV || 'development';

// All configurations will extend these options
// ============================================
let all = {
    env: env,
    
    // Server port
    //port: process.env.PORT || 9000,
    
    // Mongoose connection 
    mongoose: {
        uri: 'mongodb://lapr5:lapr5@ds161400.mlab.com:61400/fornecedor'
        
    },

    jwt: {
        secret   : "fSk35bzq6KutR0dQVKTL",
        issuer   : "http://projeto.arqsi.local",
        audience : "Everyone"
    },

    gestaoReceitas: {
        url      : 'https://gestaoreceitaslapr5.azurewebsites.net',
        email    : 'email@example.com',
        password : 'password',
        token    : ''
    },

    mail: {
        host     : 'mail.smtp2go.com',
        port     : 2525,
        username : 'lapr5',
        password : 'NnZxbTJqYnU4d3Aw'
    },

    gestaoFarmacia:{
        url      : 'https://gerirfarmaciaapi.azurewebsites.net',
        email    : 'email@example.com',
        password : 'password',
        token    : ''
    }
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
    all,
    require(`./${env}.js`)
);