var Client = require('node-rest-client').Client;
var client = new Client();
var token = '';
const config = require('../config/environment');

/*function getToken(callback) {

    var url = config.gestaoReceitas.url + "/api/auth/login";
    var args = {
        data: {"email": config.gestaoReceitas.email,"password":config.gestaoReceitas.password} ,
        headers: { "Content-Type": "application/json" }
    };

    client.post(url, args, function (data, response) {
        config.gestaoReceitas.token = data.token;
        //console.log(this.token);
        return callback(data);
    });
}

function getData(path, callback) {
        var args = {
            headers: { "Content-Type": "application/json", "Authorization": "Bearer " + config.gestaoReceitas.token }
        };
        var url = config.gestaoReceitas.url + path;
        client.get(url, args, function (data, response) {
            return callback(data);
        });

    console.log("GetData");
}*/

function putData(path,info, callback) {
    
    console.log("PostData");

    var url = config.gestaoFarmacias.url + path;
    var args = {
        data: info,
        headers: { "Content-Type": "application/json"}
    };

    client.put(url, args, function (data, response) {
        console.log(data);
        return callback(data);
    });
}

//exports.getData = getData;
exports.putData = putData;
//exports.getToken = getToken;