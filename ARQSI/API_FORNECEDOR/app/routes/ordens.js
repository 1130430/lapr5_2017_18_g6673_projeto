const Ordem      = require('../models/Ordem');
var async = require('async');
const utils     = require('../components/utils');
var express = require('express');
var router = express.Router();
const log4js = require('log4js');
log4js.configure({
    appenders: {
      app: { type: 'file', filename: './log/app.log', maxLogSize: 10485760, numBackups: 3 }
    },
    categories: {
      default: { appenders: ['app'], level: 'info' }
    }
  });

const logger = log4js.getLogger('info');

router.get('/',function(req, res){
    Ordem.find(function(err,ordens){
        if(err) 
            res.send(err);
        res.json(ordens);
    });
})

router.post('/',function (req,res){
    logger.info("A receber ordem de encomenda...");
    let ordem            = new Ordem();
    ordem.nomeFarmacia =req.body.nomeFarmacia;
    let date = new Date(Date.now());
    let hour = date.getHours();
    console.log(hour);
    if(hour<12){
        ordem.prioridade = "Manha"
    }else{
        ordem.prioridade = "Tarde"
    }
    let data = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
    ordem.data = data;
    ordem.save()
    .then(r => {
        return res.status(201).json(r);
    })
    .catch(utils.handleError(req, res));
})

router.get('/list',function(req, res){
    logger.info("A listar as ordens de encomenda...");
    var date = new Date(Date.now());
    date.setDate(date.getDate()-1);
    
    let data = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
    console.log(data);
    let query = {data : data };

    Ordem.find(query)
    .then(ordens => res.status(200).json(ordens))
    .catch(utils.handleError(req, res));

})

router.post('/runprogram',function(req,res){
    const { exec } = require('child_process');
    exec('my.bat', (err, stdout, stderr) => {
      if (err) {
        console.error(err);
        return;
      }
      console.log(stdout);
    });
    res.json("deu!");
})

module.exports = router;