//import { forEach } from '../../../../../../../../../AppData/Local/Microsoft/TypeScript/2.6/node_modules/@types/async';

var express = require('express');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var app = express()
var router = express.Router();
var fs = require('fs'); 
var ordens= require('../routes/ordens');
var Client = require('node-rest-client').Client;
var client = new Client();
const config = require('../config/environment');
const Ordem      = require('../models/Ordem');
const log4js = require('log4js');
log4js.configure({
    appenders: {
      app: { type: 'file', filename: './log/app.log', maxLogSize: 10485760, numBackups: 3 }
    },
    categories: {
      default: { appenders: ['app'], level: 'info' }
    }
  });

const logger = log4js.getLogger('info');
//const config  = require('./app/config/environment');

// The absolute path of the new file with its name
var filepath = "file.txt";
var filepathCaminhos = "ALGAV/Caminho.txt";
let contentfile;

router.post('/ordemEncomendas', function(req,res){
    var date = new Date(Date.now());
    logger.info("A persistir ordens de encomenda para a data " + date);
    date.setDate(date.getDate()-1);
    
    let data = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
    console.log(data);
    let query = {data : data };
    let content = '';
    var nomesFarmacias;

    Ordem.find(query)
    .then(ordens =>  {
        // Filter the prescriptions that need to be notified
        for(let ordem of ordens){
            content += "ordem(\'"+ordem.nomeFarmacia+"\',\'"+ordem.prioridade+"\').";         
        };
        fs.writeFile(filepath, content, (err) => {
            if (err) throw err;
        
            return res.status(201).json("The file was succesfully saved!");
        });
    });
})

router.get('/readfile', function(req,res){
    logger.info("Ler ficheiro das farmácias ... ");
    fs.readFile(filepathCaminhos, 'latin1', function(err, data) {  
        if (err) throw err;
        temp = data;
        //nomesFarmacias = data.split(",");
        contentfile= temp.replace(",]","]");
        console.log(contentfile);
       
  
        return res.status(201).json("The file was read succesfully!");
    });
});

router.post('/enviarFarmacias', function (req, res) {
    logger.info("A enviar nomes das farmácias ... ");
    logger.info("Ler ficheiro das farmácias ... ");
    fs.readFile(filepathCaminhos, 'latin1', function(err, data) {
        if (err) throw err;
        var teste = data;
        var content= teste.replace(",]","]");
        console.log("PostData");
        var url = "https://gerirfarmaciaapi.azurewebsites.net/api/farmacias/refillStock";
        var toSend='{"farmacias":'+content+'}';
        var data=JSON.parse(toSend);

        var args = {
            data,
            headers: { "Content-Type": "application/json" }
        };
        client.put(url, args, function (data, response) {
            console.log(data);
            res.status(200).json(data);
        });
    });
});


module.exports = router;