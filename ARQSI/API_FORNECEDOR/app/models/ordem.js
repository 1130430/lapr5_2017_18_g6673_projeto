const mongoose    = require('mongoose');
const Schema      = mongoose.Schema;
//const idValidator = require('mongoose-id-validator');
const timestamps  = require('../components/timestamps');

let OrdemSchema = new Schema({
    nomeFarmacia: String,
    prioridade: String,
    data : String
});

module.exports = mongoose.model('Ordem', OrdemSchema);