?-[library(rdf)].

:- consult('ordem.txt').
:- use_module(library(sgml)).
:- use_module(xml).


:-initialization start.

degrees2radians(Deg,Rad):-
	Rad is Deg*0.0174532925.

% distance(latitude_first_point, longitude_first_point, latitude_second_point, longitude_second_point, distance in meters)
distance(Lat1, Lon1, Lat2, Lon2, Dis2):-
	degrees2radians(Lat1,Psi1),
	degrees2radians(Lat2,Psi2),
	DifLat is Lat2-Lat1,
	DifLon is Lon2-Lon1,
	degrees2radians(DifLat,DeltaPsi),
	degrees2radians(DifLon,DeltaLambda),
	A is sin(DeltaPsi/2)*sin(DeltaPsi/2)+ cos(Psi1)*cos(Psi2)*sin(DeltaLambda/2)*sin(DeltaLambda/2),
	C is 2*atan2(sqrt(A),sqrt(1-A)),
	Dis1 is 6371000*C,
	Dis2 is round(Dis1).

% inicia a leitura do ficheiro xml
start:-
	load_xml_file('file.xml',XML),
	XML=[element('osm',_,Kids0)],tratarXML(Kids0),!,leitura,!
	,exportFarmacias,halt.

leitura:-
	getListaFarmacias,getListaNodes,getListaWays,getListaRelation.

% remove tudo o que nao interessa
tratarXML([]):-!.
tratarXML([H|T]):- ((compare(H,'\n'); compare(H,'\n\t'); compare(H,'\n\n'); compare(H,'\n\n  ');compare(H,'\n  '); compare(H,'\n\t  '))
						, tratarXML(T); tratarElem(H), tratarXML(T)).

		
% Insere os elementos na base de conhecimento
tratarElem(element(N,S,L)):- corrigeLista(L,[],NL),assertz(element(N,S,NL)).

compare(H,H).


% Remove informa��o inutil da lista
corrigeLista([],LA,LA).
corrigeLista([H|T],LA,LF):- ((compare(H,'\n    ');compare(H,'\n'); compare(H,'\n\t'); compare(H,'\n\n'); compare(H,'\n\n  ');compare(H,'\n  '); compare(H,'\n\t  ')), corrigeLista(T,LA,LF)
							;
							corrigeLista(T,[H|LA],LF)).
							
							
							
                            
% Esta a ser guardado com o formato element(tipo, Lista de carateristicas, Lista de sub-nos).

											
isEmpty([]).




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Obtem o caminho mais curto para a viagem do dia
caminhoCurto(Orig, Cam, Custo):- 	%parte da manha
									findall(ID, (ordem(F,'Manha'), farmacia(F,ID)), ListaNos),
									findall(ID, (ordem(F,'Tarde'), farmacia(F,ID)), ListaNosTarde),
									%Se nao houver de manha
									(isEmpty(ListaNos), 
										CamAux = [], Custo1 is 0,
										
										%Se nao houver de Tarde
										(isEmpty(ListaNosTarde), Cam = [], Custo is 0,!, true,!
										;
										parteTarde(Orig,ListaNosTarde,CamAux2,Custo2))
									;
									parteManha(Orig, ListaNos,CamAux,Custo1),
										
										%Se nao houver de tarde
										(isEmpty(ListaNosTarde), CamAux2=[], Custo2 is 0
										;
										pickLast(CamAux,Act),
										parteTarde(Act,ListaNosTarde,CamAux2,Custo2))
									),
									%voltar ao armazem
									(isEmpty(CamAux2),
										pickLast(CamAux,Act2)
									;
									pickLast(CamAux2,Act2) 
									), 
									cc(Orig,[(_,0,[Act2])],CamRetorno,CustoRetorno),
									
									%Juntar tudo
									append(CamAux,CamAux2,CamResultado),append(CamResultado,CamRetorno,CamAux3), Custo is (Custo1 + Custo2) + CustoRetorno,
									Cam = [Orig|CamAux3].
									
									
pickLast(List,Last):- reverse(List,Aux), Aux=[Last|_].									
									
parteManha(Orig, ListaNos, Cam1, Custo1):-			ordenaOrdens(Orig,ListaNos,[Orig],LF), reverse(LF, LOrdenada),!,
														printAllPharmacy(LOrdenada),
														fazListaCaminho(LOrdenada,0,[],Cam1,Custo1).
														
														

parteTarde(Orig,ListaNosTarde,Cam2,Custo2):-			ordenaOrdens(Orig,ListaNosTarde,[Orig],LF1), reverse(LF1, LOrdenada1),!,
														LOrdenada1 = [Head|Tail],write(','),
														printAllPharmacy(Tail),
														fazListaCaminho(LOrdenada1,0,[],Cam2,Custo2).
														
																									
									
								
fazListaCaminho([H],Custo,Cam,Cam,Custo).								
fazListaCaminho([H,D|T],CustoAux,CamAux,Cam,Custo):- cc(D,[(_,0,[H])],CamAdd,CustoAdd), append(CamAux,CamAdd,CamNew),CustoNew is CustoAux + CustoAdd,
														fazListaCaminho([D|T],CustoNew,CamNew,Cam,Custo).									
									

remove_first_element([H],[]).
remove_first_element([H|T],[H|Lista]):-
	remove_first_element(T,Lista).
									
cc(Dest,[(_,Custo,[Dest|T])|_],Cam,Custo):- 	remove_first_element([Dest|T],Cam1),
												reverse(Cam1,Cam).		
												
cc(Dest,[(_,Ca,LA)|Outros],Cam,Custo):- LA = [Act|_],
										findall((CEX,CaX,[X|LA]),
										(Dest\==Act,getWayWithNode(Act,List),hasWay(List,W),way(W,WL),hasWay(WL,X),
											\+member(X,LA),estimativa(X,Act,CustoX), CaX is CustoX + Ca, estimativa(X,Dest,EstX), CEX is CaX + EstX),
										Novos),
										append(Outros,Novos,Todos), sort(Todos,TodosOrd), cc(Dest,TodosOrd,Cam,Custo).
																		
										
hasWay(List,X):- member(X,List).									
										
%Estimativa da distacia entre dois n�s em linha reta										
estimativa(ID1,ID2,Est):- node(ID1,X1,Y1), node(ID2,X2,Y2), 
						distance(X1,Y1,X2,Y2,Est1), Est is Est1/1000.
						
						%DX is X1-X2, DY is Y1-Y2, 
						%Est is sqrt(DX*DX+DY*DY). 


				
%Ordena as ordens por estimativa de distancia				
ordenaOrdens(_,[],LF,LF).										
ordenaOrdens(Orig,LO,LA,LF):- LO=[Aux|T], estimativa(Orig,Aux,Est),maisProximo(Orig,T,Est,Aux,Res), delete(LO,Res,LO2),
(getWayWithNode(Res,L), isEmpty(L), ordenaOrdens(Orig,LO2,LA,LF); ordenaOrdens(Res,LO2,[Res|LA],LF)).

%Obtem o proximo n� mais proximo relativo ao atual
maisProximo(_,[],_,Res,Res).
maisProximo(Orig,[H|T],Dist,Aux,Res):- estimativa(Orig,H,Est), 
										(Est<Dist, maisProximo(Orig,T,Est,H,Res)
										;
										maisProximo(Orig,T,Dist,Aux,Res)).
										
										
										
getWayWithNode(NodeID, ListW):- findall(ID, (way(ID,List), member(NodeID,List)),ListW).		

printAllPharmacy([]).
printAllPharmacy([H|T]):-
	isNodePharmacy(H,X),write('"'),write(X),write('"'),(\+isEmpty(T),write(',')),
	printAllPharmacy(T)
	;
	printAllPharmacy(T).

isNodePharmacy(X,H):-
	farmacia(H,X)
	;
	fail.
	
exportFarmacias:-
	tell('Caminho.txt'),write('['),caminhoCurto(115,X,Y),write(']'),told,
	tell('NosECusto.txt'),write('Caminho:'),nl,write(X),nl,write('Custo:'),nl,write(Y), write(' Km'),told.
	
	
	