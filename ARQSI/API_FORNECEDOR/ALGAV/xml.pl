:- module(xml, [getListaFarmacias/0,getListaNodes/0,getListaWays/0,getListaRelation/0,farmacia/2,node/3,way/2,relation/3,ordem/2]).

getWayWithNode(NodeID, ListW):- findall(ID, (way(ID,List), member(NodeID,List)),ListW).			

isEmpty([]).

atom_converter([],[]).		
atom_converter([H|T],[H1|ListaS]):-
	atom_converter(T,ListaS),
	atom_number(H,H1).
	
insereFarmaciasBC([(ID,_,_)|[X]]):-
	member(element(tag,[k='name' ,v=Y],_),X),
	atom_number(ID,ID1),
	assertz(farmacia(Y,ID1)).
	
insereRelationBC(Id,Nos):-
	findall(X,member(element(member,[type=_ , ref=X, role=_],_),Nos),LN),
	member(element(tag,[k='restriction', v=Z],_),Nos),
	atom_number(Id,Id1),
	atom_converter(LN,LN1),
	assertz(relation(Id1,LN1,Z)).

insereWayBc(Id,NdList):-
	findall(X,member(element(nd,[ref=X],_),NdList),LX),
	atom_number(Id,Id1),atom_converter(LX,LX1),
	sort(LX1,LX2),
	assertz(way(Id1,LX2)),
	member(element(tag,[k='amenity' ,v='pharmacy'],_),NdList),
	member(element(tag,[k='name' ,v=Name],_),NdList),
	member(First,LX1),!,
	assertz(farmacia(Name,First)),
	assertz(ordem(Name,'Manha'))
	.
		
getListaFarmacias:-
	findall([(Z,Lat,Lon),Y],(element(node,[id=Z , lat=Lat , lon=Lon , version=_ ,timestamp=_ ,changeset=_ ,uid=_ , user=_ ],Y),
	member(element(tag,[k='amenity' , v='pharmacy'],_),Y)),ListaF),
	member(X,ListaF),insereFarmaciasBC(X),fail;true.

getListaNodes:-
	findall((Id,Lat,Lon),element(node,[id=Id , lat=Lat , lon=Lon , version=_ ,timestamp=_ ,changeset=_ ,uid=_ , user=_ ],_),LN),
	member((X,Y,Z),LN),atom_number(X,X1),atom_number(Y,Y1),atom_number(Z,Z1),assertz(node(X1,Y1,Z1)),fail;true.
	
getListaWays:-
	findall((Id,Nd),element(way,[id=Id ,version=_, timestamp=_ , changeset=_ , uid=_, user=_],Nd),LW),
	member((X,Y),LW),insereWayBc(X,Y),fail;true.
	
getListaRelation:-
	findall((Id,Nos),(element(relation,[id=Id, version=_ , timestamp=_ , changeset=_ , uid=_ , user=_],Nos),member(element(tag,[k='type', v='restriction'],_),Nos)),LNW),
	member((X,Y),LNW),insereRelationBC(X,Y),fail;true.
	
