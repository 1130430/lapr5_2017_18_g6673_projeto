export const environment = {
  production: true,
  apiUrl     : 'https://gestaoreceitaslapr5.azurewebsites.net/api',
  socketUrl  : 'gestaoreceitaslapr5.azurewebsites.net'
};
