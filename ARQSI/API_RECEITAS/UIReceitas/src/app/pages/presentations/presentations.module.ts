import { NgModule } from '@angular/core';

import { PresentationsComponent } from './presentations.component';

import { PresentationsRoutingModule } from './presentations-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [ PresentationsRoutingModule, SharedModule ],
    declarations: [
        PresentationsComponent,
    ]
})
export class PresentationsModule { }
