import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Prescricao, Aviamento } from '../../models';
import { DispatchPrescriptionComponent } from '../../dialogs/dispatch-prescription/dispatch-prescription.component';
import * as moment from 'moment';

@Component({
    selector: 'app-prescriptions',
    templateUrl: './prescriptions.component.html',
    styleUrls: ['./prescriptions.component.scss']
})
export class PrescriptionsComponent implements OnInit {
    prescricoes : Array<Prescricao> = [];
    loading     : boolean           = false;
    bsConfig    : Partial<BsDatepickerConfig>;
    minDate     : moment.Moment = moment();
    searchValue : Date;
    constructor(
        private http: HttpClient,
        private toastr: ToastrService,
        private modalService: BsModalService
    ) { 
        this.bsConfig = { showWeekNumbers: false, dateInputFormat: "DD/MM/YYYY"};
    }
    
    ngOnInit() {
        this.loadPrescriptions();
    }

    showDispatches(data: Prescricao) {
        let ref = this.modalService.show(DispatchPrescriptionComponent, {class: 'modal-lg'});
        ref.content.prescricao = data;
    }

    clearSearch() {
        this.searchValue = null;
    }

    search(event) {
        this.loadPrescriptions();
    }

    private loadPrescriptions() {
        this.loading = true;
        let search = this.searchValue ? moment(this.searchValue).format("YYYY-MM-DD") : "";
        this.http.get<Prescricao[]>(`utentes/prescricoes/poraviar`, {
            params: {
                data: search
            }
        }).subscribe(
            data => {
                this.loading = false;
                this.prescricoes = data;
            },
            err => this.handleError(err)
        );
    }

    private handleError(err) {
        this.loading = false,
        this.toastr.error(err.error.message, 'Erro');
    }
    
}
