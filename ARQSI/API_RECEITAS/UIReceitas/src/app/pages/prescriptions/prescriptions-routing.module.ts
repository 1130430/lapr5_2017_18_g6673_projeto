import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrescriptionsComponent } from './prescriptions.component';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Prescrições'
        },
        component: PrescriptionsComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PrescriptionsRoutingModule {}
