import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../services/auth.service';
import { Receita } from '../../models/index';
import { Prescricao } from '../../models/Prescricao';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    public user = { email: '', name: '' };
    public med = { medicamento: '', quantidade: 0 };
    public medTomado = { medicamento: '', quantidade: 0 };
    public info = { nome: '', quantidade: 0 };
    model: any = {}; 
    public choose = ['Fármaco','Medicamento'];


    public aviamentos = 0;
    pies = {
        prescricoes: {
            labels: ["Por aviar", "Parcialmente aviadas", "Completamente aviadas"],
            data: [0, 0, 0],
            loading: false
        },
        utente: {
            labels: ["Hoje", "Amanhã", "Esta semana", "Depois"],
            data: [0, 0, 0, 0],
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    enabled: false
                }
            },
            loading: false
        },
        farmacos: {
            labels: [],
            data: [],
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    enabled: false
                }
            },
            loading: false
        }
    };
    constructor(
        private http: HttpClient,
        public authService: AuthService,
        private toastr: ToastrService
    ) { }

    ngOnInit() {
        this.loadData();

    }

    chooseData(){
        if(this.model.data=='Fármaco'){
            this.farmacoMaisPrescrito();
            console.log(this.info);
        }else{
            this.maisPrescrito();
            console.log(this.info);
        }
    }

    loadData() {
        this.loadPrescriptions();
        this.user = this.authService.getUser();
        if (this.authService.getUser().role == 'medico') {
           // this.farmacoMaisPrescrito();
        }
        if (this.authService.getUser().role == 'utente') {
            this.loadUserPrescriptions();
            this.maisTomado();
        }
        if(this.authService.getUser().role == 'medico'){
            this.loadFarmacos();}
    }

    private loadPrescriptions() {
        this.pies.prescricoes.loading = true;
        this.http.get<any>('chart/prescricoes').subscribe(
            response => {
                let data = [response.por_aviar, response.parcial, response.aviado];
                // Need to replace the original array otherwise the chart wont update
                this.pies.prescricoes.data = data;
                this.pies.prescricoes.loading = false;
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
                this.pies.prescricoes.loading = false;
            }
        );
    }

    private maisTomado() {

        this.http.get<any>('chart/medicamentos/maistomado', {
            params: {
                utente: this.authService.getUser().id
            }
        }).subscribe(
            response => {
                console.log(response);
                this.medTomado = response;
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
            }
            );

    }

    private maisPrescrito() {
        
                this.http.get<any>('chart/medicamentos', { 
                    params: {
                        medico: this.authService.getUser().id
                    }
                }).subscribe(
                    response => {
                     
                        this.info = response;console.log(this.info);
                    },
                    err => {
                        this.toastr.error(err.error.message, 'Erro');
                    }
                    );
        
            }
        

    private loadFarmacos() {
        //var f = this.authService.getFarmacia();
        this.pies.farmacos.loading = true;
        this.http.get<any>('chart/farmacos').subscribe(
            s => {console.log(s.farmaco);
                    console.log(s.quantidade);
                    this.pies.farmacos.data=s.quantidade;
                    this.pies.farmacos.labels=s.farmaco;
                    console.log(this.pies.farmacos.data);
                    console.log(this.pies.farmacos.labels);
                this.pies.farmacos.loading = false;
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
                this.pies.farmacos.loading = false;
            }
        );
    }

    private loadUserPrescriptions() {
        this.pies.utente.loading = true;
        this.http.get<Prescricao[]>('utentes/prescricoes/poraviar')
            .subscribe(
            prescricoes => {
                let today = moment();
                let data = [0, 0, 0, 0];

                prescricoes.forEach(p => {
                    let date = moment(p.dataValidade);
                    if (date.isSame(today, 'day')) {
                        data[0] += 1;
                    } else if (today.clone().add(1, 'days').isSame(date, 'day')) {
                        data[1] += 1;
                    } else if (today.clone().endOf('week').isAfter(date, 'day')) {
                        data[2] += 1;
                    } else {
                        data[3] += 1;
                    }
                });

                this.pies.utente.data = data;
                this.pies.utente.loading = false;
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
                this.pies.utente.loading = false;
            }
            );
    }


    private farmacoMaisPrescrito() {
        this.http.get<any>('chart/medicamentos/farmacomaisprescrito', {
            params: {
                medico: this.authService.getUser().id
            }
        }).subscribe(
            response => {
                this.info = response;console.log(this.info);
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
            }
            );
    }


}
