const router     = require('express').Router();
const controller = require('./medicamentos.controller');

router.get('/', controller.list);
router.get('/:id/posologias', controller.posologias);

module.exports = router;