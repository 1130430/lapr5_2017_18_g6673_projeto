const utils = require('../../components/utils');
const gdm   = require('../../components/gestao-medicamentos');
const log4js = require('log4js');
log4js.configure({
    appenders: {
      app: { type: 'file', filename: './log/app.log', maxLogSize: 10485760, numBackups: 3 }
    },
    categories: {
      default: { appenders: ['app'], level: 'info' }
    }
  });

const logger = log4js.getLogger('info');

function list(req, res){
    logger.info("A listar todos os medicamentos...");
    gdm.get('/api/medicamentos')
    .then(response => res.status(200).json(response.data))
    .catch(utils.handleError(req, res));
}

function posologias(req, res){
    logger.info("A listar as posologias para o medicamento " + req.params.id);
    gdm.get(`/api/medicamentos/${req.params.id}/posologias`)
    .then(response => res.status(200).json(response.data))
    .catch(utils.handleError(req, res));
}

module.exports = {
    list       : list,
    posologias : posologias
};