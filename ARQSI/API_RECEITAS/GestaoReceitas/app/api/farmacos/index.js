const router     = require('express').Router();
const controller = require('./farmacos.controller');
const auth       = require('../middleware/auth.middleware');

router.get('/', controller.list);

module.exports = router;