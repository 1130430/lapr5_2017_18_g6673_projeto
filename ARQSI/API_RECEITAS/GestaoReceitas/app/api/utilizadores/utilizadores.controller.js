const utils = require('../../components/utils');
const User = require('../../models/User');
const log4js = require('log4js');
log4js.configure({
    appenders: {
      app: { type: 'file', filename: './log/app.log', maxLogSize: 10485760, numBackups: 3 }
    },
    categories: {
      default: { appenders: ['app'], level: 'info' }
    }
  });

const logger = log4js.getLogger('info');

function list(req, res){
    let query = {};
    logger.info("A mostrar todos os utilizadores...");
    if(req.query.role){
        query.role = req.query.role;
    }
    User.find(query)
    .then(users => res.status(200).json(users))
    .catch(utils.handleError(req, res));
}

module.exports = {
    list : list
};