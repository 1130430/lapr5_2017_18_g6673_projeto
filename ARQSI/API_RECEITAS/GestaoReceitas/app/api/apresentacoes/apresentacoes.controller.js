const utils = require('../../components/utils');
const gdm   = require('../../components/gestao-medicamentos');
const moment = require('moment');
const log4js = require('log4js');
log4js.configure({
    appenders: {
      app: { type: 'file', filename: './log/app.log', maxLogSize: 10485760, numBackups: 3 }
    },
    categories: {
      default: { appenders: ['app'], level: 'info' }
    }
  });

const logger = log4js.getLogger('info');

function list(req, res){
    logger.info("A mostrar todas as apresentações...");
    gdm.get('/api/apresentacoes')
    .then(response => res.status(200).json(response.data))
    .catch(utils.handleError(req, res));
}

function listComments(req, res){
    logger.info("A mostrar todos os comentários para a apresentação com o id " + req.params.id);
    gdm.get(`/api/apresentacoes/${req.params.id}/comentarios`)
    .then(response => {
        //Transform the dates from DD/MM/YYYY to YYYY/MM/DD to follow the standard in the other controllers
        const data = response.data.map(c => {
            c.data = moment(c.data, 'DD/MM/YYYY').format('YYYY/MM/DD');
            return c;
        });
        return res.status(200).json(data);
    })
    .catch(utils.handleError(req, res));
}

function createComment(req, res){
    logger.info("A criar comentário...");
    req.body.data = req.body.data || moment().format('DD/MM/YYYY');
    req.body.apresentacaoId = req.params.id;

    gdm.post(`/api/apresentacoes/${req.params.id}/comentarios`, req.body)
    .then(response => res.status(200).json(response.data))
    .catch(utils.handleError(req, res));
}

module.exports = {
    list          : list,
    listComments  : listComments,
    createComment : createComment
};