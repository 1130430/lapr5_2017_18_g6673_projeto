const router     = require('express').Router();
const controller = require('./apresentacoes.controller');
const auth       = require('../middleware/auth.middleware');

router.get('/', controller.list);
router.get('/:id/comentarios', controller.listComments);
router.post('/:id/comentarios', auth.hasRole('medico'), controller.createComment);

module.exports = router;