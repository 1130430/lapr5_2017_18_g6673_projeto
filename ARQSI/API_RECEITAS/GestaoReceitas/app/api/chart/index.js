const router     = require('express').Router();
const controller = require('./chart.controller');
const auth       = require('../middleware/auth.middleware');

router.get('/prescricoes', controller.prescricoes);
router.get('/prescricoes/poraviar', auth.isAuthenticated(), controller.porAviar);
router.get('/prescricoesFarmaceutico', controller.prescricoesFarmaceutico);
router.get('/medicamentos/maisaviado', controller.medicamentoMaisAviado);
router.get('/medicamentos/maistomado', controller.medicamentoMaisTomado);
router.get('/medicamentos/farmacomaisprescrito', controller.farmacoMaisPrescrito);
router.get('/farmacos', controller.farmacos);
router.get('/medicamentos', controller.medicamentoMaisPrescrito);

module.exports = router;