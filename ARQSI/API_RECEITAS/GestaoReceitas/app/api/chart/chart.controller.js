const utils = require('../../components/utils');
const Receita = require('../../models/Receita');
const gdm = require('../../components/gestao-medicamentos');
const moment = require('moment');
async = require("async");
var wait = require('wait-promise');

function prescricoes(req, res) {
    Receita.find({})
        .then(receitas => {
            let data = {
                por_aviar: 0,
                parcial: 0,
                aviado: 0
            };

            receitas.forEach(r => {
                r.prescricoes.forEach(p => {
                    if (p.quantidade === 0) {
                        data.aviado += 1;
                    } else if (p.aviamentos.length > 0) {
                        data.parcial += 1;
                    } else {
                        data.por_aviar += 1;
                    }
                });
            });

            return res.status(200).json(data);
        })
        .catch(utils.handleError(req, res));
}

function medicamentoMaisAviado(req, res) {
    let data = {
        medicamento: '',
        quantidade: 0
    };
     flag = true;
    let count = 0;
    var med = 0;
    gdm.get('/api/medicamentos')
        .then(response => {
            response.data.forEach(m => {
                var tam = 0;
                med++;
                count = 0;
                Receita.find({})
                    .then(receitas => {
                        receitas.forEach(r => {
                            r.prescricoes.forEach(p => {
                                if (p.idMedicamento == m.id) {
                                    p.aviamentos.forEach(a => {
                                        count += a.quantidade;
                                    });
                                }
                            });
                            tam ++;
                            if(count > data.quantidade && tam == receitas.length){
                                data.quantidade = count;
                                data.medicamento = m.nome;
                            }
                            setTimeout(function() {
                            if(med == response.data.length && tam == receitas.length){
                                if(flag){
                                    flag = false; 
                                    return res.status(200).json(data);
                                }
                            }
                        }, 1000);
                        });
                    });
            });
        })
        .catch(utils.handleError(req, res));
}

function farmacoMaisPrescrito(req, res) {
    let data = {
        nome: '',
        quantidade: 0
    };
     flag = true;
    let count = 0;
    var med = 0;
    gdm.get('/api/farmacos')
        .then(response => {
            
            response.data.forEach(f => {
                var tam = 0;
                med++;
                count = 0;
                Receita.find({medico: req.query.medico})
                    .then(receitas => {
                        receitas.forEach(r => {
                            r.prescricoes.forEach(p => {
                                if (p.idFarmaco == f.id) {
                                   
                                        count += 1;
                                    
                                }
                            });
                            tam ++;
                            if(count > data.quantidade && tam == receitas.length){
                                data.quantidade = count;
                                data.nome = f.nome;
                            }
                            setTimeout(function() {
                            if(med == response.data.length && tam == receitas.length){
                                if(flag){
                                    flag = false; 
                                    return res.status(200).json(data);
                                }
                            }
                        }, 1000);
                        });
                    });
            });
        })
        .catch(utils.handleError(req, res));
}

function farmacos(req, res) {
    let data = {
        
        farmaco: [],
        quantidade: []
        
    };
     flag = true;
    let count = 0;
    var med = 0;
    gdm.get('/api/farmacos')
        .then(response => {
            
            response.data.forEach(f => {
                var tam = 0;
                med++;
                count = 0;
                Receita.find({})
                    .then(receitas => {
                        receitas.forEach(r => {
                            r.prescricoes.forEach(p => {
                                if (p.idFarmaco == f.id) {
                                        count += 1;
                                }
                            });
                            tam ++;
                            if(tam == receitas.length){
                                data.quantidade.push(count);
                                data.farmaco.push(f.nome);
                                count=0;
                            }
                            setTimeout(function() {
                            if(med == response.data.length && tam == receitas.length){
                                if(flag){
                                    flag = false; 
                                    return res.status(200).json(data);
                                }
                            }
                        }, 1000);
                        });
                    });
            });
        })
        .catch(utils.handleError(req, res));
}


function medicamentoMaisTomado(req, res) {
    let data = {
        medicamento: '',
        quantidade: 0
    };
     flag = true;
    let count = 0;
    var med = 0;
    let sleep = wait.sleep;
    gdm.get('/api/medicamentos')
        .then(response => {
            response.data.forEach(m => {
                var tam = 0;
                med++;
                count = 0;
                Receita.find({utente: req.query.utente})
                    .then(receitas => {
                        if(receitas.length==0) return res.status(200).json(data);
                        receitas.forEach(r => {
                                r.prescricoes.forEach(p => {
                                    if (p.idMedicamento == m.id) {
                                        p.aviamentos.forEach(a => {
                                            count += a.quantidade;
                                        });
                                    }
                                });
                                
                                tam ++;
                                
                                    if(count > data.quantidade && tam == receitas.length){
                                        data.quantidade = count;
                                        data.medicamento = m.nome;
                                    }
                                    setTimeout(function() {
                                    if(med == response.data.length && tam == receitas.length){
                                        
                                        if(flag){
                                            flag = false; 
                                            return res.status(200).json(data);
                                        }
                                    } 
                                }, 1000);
                                    
                        });
                    })
                
            });
        })
        .catch(utils.handleError(req, res));

}

function medicamentoMaisPrescrito(req, res) {
    let data = {
        nome: '',
        quantidade: 0
    };
     flag = true;
    let count = 0;
    var med = 0;
    gdm.get('/api/medicamentos')
        .then(response => {
            
            response.data.forEach(f => {
                var tam = 0;
                med++;
                count = 0;
                Receita.find({medico: req.query.medico})
                    .then(receitas => {
                        receitas.forEach(r => {
                            r.prescricoes.forEach(p => {
                                if (p.idMedicamento == f.id) {
                                   
                                        count += 1;
                                    
                                }
                            });
                            tam ++;
                            if(count > data.quantidade && tam == receitas.length){
                                data.quantidade = count;
                                data.nome = f.nome;
                            }
                            setTimeout(function() {
                            if(med == response.data.length && tam == receitas.length){
                                if(flag){
                                    flag = false; 
                                    return res.status(200).json(data);
                                }
                            }
                        }, 1000);
                        });
                    });
            });
        })
        .catch(utils.handleError(req, res));

}

function prescricoesFarmaceutico(req, res) {
    console.log(req.query.farmaceutico);
    Receita.find({})
        .then(receitas => {
            let data = 0;

            receitas.forEach(r => {
                r.prescricoes.forEach(p => {
                    p.aviamentos.forEach(a => {

                        if (a.farmaceutico == req.query.farmaceutico) {

                            data += 1;
                        }
                    });
                });
            });
            return res.status(200).json(data);
        })
        .catch(utils.handleError(req, res));
}

function porAviar(req, res) {
    let query = {
        utente: req.user,
        'prescricoes.quantidade': { '$gt': 0 }
    };
    Receita.find(query)
        .then(receitas => {
            let data = {
                hoje: 0,
                amanha: 0,
                esta_semana: 0,
                mais_tarde: 0
            };
            let today = moment();

            // Filter the prescriptions
            receitas.forEach(r => {
                r.prescricoes.forEach(p => {
                    if (p.quantidade == 0) return;
                    let date = moment(p.dataValidade);
                    if (date.isSame(today, 'day')) {
                        data.hoje += 1;
                    } else if (today.clone().add(1, 'days').isSame(date, 'day')) {
                        data.amanha += 1;
                    } else if (date.isBefore(today.clone().endOf('isoWeek'), 'day')) {
                        data.esta_semana += 1;
                    } else {
                        data.mais_tarde += 1;
                    }
                });
            });
            return res.status(200).json(data);
        }).catch(utils.handleError(req, res));
}

module.exports = {
    prescricoes: prescricoes,
    porAviar: porAviar,
    prescricoesFarmaceutico: prescricoesFarmaceutico,
    medicamentoMaisAviado : medicamentoMaisAviado,
    medicamentoMaisTomado : medicamentoMaisTomado,
    farmacoMaisPrescrito: farmacoMaisPrescrito,
    farmacos: farmacos,
    medicamentoMaisPrescrito: medicamentoMaisPrescrito
};