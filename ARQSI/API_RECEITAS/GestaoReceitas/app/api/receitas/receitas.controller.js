const moment    = require('moment');
const utils     = require('../../components/utils');
const Receita   = require('../../models/Receita');
const User      = require('../../models/User');
const mailer    = require('../../components/mailer');
const websocket = require('../../components/websocket');
const log4js = require('log4js');
log4js.configure({
    appenders: {
      app: { type: 'file', filename: './log/app.log', maxLogSize: 10485760, numBackups: 3 }
    },
    categories: {
      default: { appenders: ['app'], level: 'info' }
    }
  });

const logger = log4js.getLogger('info');

function notifyUser(receita){
    logger.info("A notificar utilizador " + receita.utente + " sobre a criação de uma receita a ele agregada...");
    return User.findById(receita.utente)
    .then(user => {
        mailer.sendMail({
            to      : user.email,
            subject : 'Nova receita',
            text    : `
            Ex.ᵐᵒ, Ex.ᵐᵃ ${user.name}
            
            Informamos que foi criada uma receita para si com o código '${receita._id}'
            
            Melhores cumprimentos.`
        });

        websocket.notify(user, {text: "Foi adicionada uma receita para si", id: receita._id});
    });
}

function index(req, res){
    let query = {};
    switch(req.user.role){
        case 'medico' : 
        query.medico = req.user;
        break;
        case 'utente':
        query.utente = req.user;
        break;
    }
    
    Receita.find(query)
    .populate('medico')
    .populate('utente')
    .populate('prescricoes.aviamentos.farmaceutico')
    .then(receitas => {
        return res.status(200).json(receitas);
    })
    .catch(utils.handleError(req, res));
}

function show(req, res){
    let query = {
        _id: req.params.id
    };
    logger.info("A mostrar todas as receitas...");
    switch(req.user.role){
        case 'medico' : 
        query.medico = req.user;
        break;
        case 'utente':
        query.utente = req.user;
        break;
    }
    Receita.findOne(query)
    .populate('medico')
    .populate('utente')
    .populate('prescricoes.aviamentos.farmaceutico')
    .then(receita => {
        if(!receita){
            return res.status(404).json({error: 'not_found', message: 'A receita não existe'});
        }
        res.status(200).json(receita);
    })
    .catch(utils.handleError(req, res));
}

function create(req,res){
    logger.info("A criar uma nova receita...");
    let receita             = new Receita();
    let prescricoes         = req.body.prescricoes || [];
    
    receita.medico      = req.user;                        //Because this route can only be accessed by medicos
    receita.utente      = req.body.utente;
    receita.prescricoes = prescricoes.map(p => {
        if(!p.idMedicamento) {
            p.idMedicamento = 0;
        }
        return p;
    });
    
    receita.save()
    .then(r => {
        notifyUser(r); //Don't need to wait for this to finish
        return res.status(201).json(r);
    })
    .catch(utils.handleError(req, res));
}

module.exports = {
    index  : index,
    show   : show,
    create : create
};