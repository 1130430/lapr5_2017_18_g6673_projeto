const moment    = require('moment');
const utils     = require('../../../components/utils');
const Receita   = require('../../../models/Receita');
const websocket = require('../../../components/websocket');
const log4js = require('log4js');
log4js.configure({
    appenders: {
      app: { type: 'file', filename: './log/app.log', maxLogSize: 10485760, numBackups: 3 }
    },
    categories: {
      default: { appenders: ['app'], level: 'info' }
    }
  });

const logger = log4js.getLogger('info');

function findPrescricao(receitaId, prescricaoId){
    logger.info("A procurar precrição com id " + prescricaoId + " para a receita com id " + receitaId);
    return new Promise((resolve, reject) => {
        Receita.findOne({_id: receitaId})
        .populate('medico')
        .populate('utente')
        .then(receita => {
            if(!receita){
                return reject({error: 'not_found', message: 'A receita não existe'});
            }
            
            return receita.prescricoes.id(prescricaoId);
        })
        .then(prescricao => {
            if(!prescricao){
                reject({error: 'not_found', message: 'A prescrição não existe'});
            }

            return resolve(prescricao);
        })
        .catch(() => reject({error: 'not_found', message: 'A receita não existe'}));
    });
}

function show(req, res){
    findPrescricao(req.params.receitaId, req.params.prescricaoId)
    .then(prescricao => res.status(200).json(prescricao))
    .catch(err => res.status(404).json(err));
}


function create(req, res){
    logger.info("A criar uma nova prescrição...");
    Receita.findOne({
        _id    : req.params.receitaId
    })
    .then(receita => {
        if(!receita){
            return res.status(404).json({error: 'not_found', message: 'A receita não existe'});
        }

        if(receita.medico.toString() != req.user._id.toString()){
            return res.status(403).json({error: 'forbidden', message: 'Você não pode criar prescrições nesta receita'});
        }

        let prescricao = receita.prescricoes.create({
            quantidade     : req.body.quantidade,
            dataValidade   : req.body.dataValidade,
            idMedicamento  : req.body.idMedicamento || 0,
            idApresentacao : req.body.idApresentacao,
            posologia      : req.body.posologia
        });

        receita.prescricoes.push(prescricao);

        receita.save()
        .then(receita => {
            websocket.notify(receita.utente, {text: "Foi adicionada uma prescrição à sua receita", id: receita._id});
            return res.status(201).json(prescricao);
        })
        .catch(utils.handleError(req, res));

    })
    .catch(utils.handleError(req, res));
}

function aviar(req, res){
    let receitaId    = req.params.receitaId;
    let prescricaoId = req.params.prescricaoId;
    logger.info("A aviar receita com id " + receitaId);

    let quantidadeParaAviar = req.body.quantidade || 0;
    if(quantidadeParaAviar == 0){
        return res.status(422).json({error: 'validation_error', message: 'Deve especificar uma quantidade superior a zero'});
    }

    Receita.findById(receitaId)
    .populate('prescricoes.aviamentos.farmaceutico')
    .then(receita => {
        if(!receita){
            return res.status(404).json({error: 'not_found', message: 'A receita não existe'});
        }

        //Aviar uma prescrição
        receita.aviar(prescricaoId, req.body.quantidade, req.user)
        .then(response => {
            return res.status(200).json(response);
        })
        .catch(err => {
            let status = err.error == 'not_found' ? 404 : 422;
            res.status(status).json(err);
        });
    })
    .catch(utils.handleError(req, res));
}

function edit(req, res){
    logger.info("A editar receita com id " + req.params.receitaId);
    Receita.findOne({
        _id    : req.params.receitaId,
        'prescricoes._id': req.params.prescricaoId
    })
    .then(receita => {
        if(!receita){
            return res.status(404).json({error: 'not_found', message: 'A prescrição não existe'});
        }

        //Make sure only the creator of the recipe can edit 
        if(receita.medico.toString() != req.user._id.toString()){
            return res.status(403).json({error: 'forbidden', message: 'Você não pode editar prescrições nesta receita'});
        }

        //Find the index of the prescription
        let index = -1;
        for(let i = 0; i < receita.prescricoes.length; i++){
            if(receita.prescricoes[i]._id == req.params.prescricaoId){
                index = i;
                break;
            }
        }

        //Check if the prescription can be edited
        if(receita.prescricoes[index].aviamentos.length > 0){
            return res.status(403).json({error: 'forbidden', message: "Já não pode editar esta prescrição"});
        }

        //Change the prescription values based on the request body
        for(let attr in req.body){
            receita.prescricoes[index][attr] = req.body[attr];
        }

        //Allow the user to get notified again
        receita.prescricoes[index].notificado = false;

        receita.save()
        .then(receita => {
            websocket.notify(receita.utente, {text: "Uma das suas prescrições foi alterada", id: receita._id});
            res.status(200).json(receita.prescricoes[index]);
        })
        .catch(utils.handleError(req, res));
        

    })
    .catch(utils.handleError(req, res));
}

module.exports = {
    show   : show,
    create : create,
    edit   : edit,
    aviar  : aviar
};