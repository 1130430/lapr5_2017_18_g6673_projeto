const router     = require('express').Router({mergeParams: true});
const controller = require('./prescricoes.controller');
const auth       = require('../../middleware/auth.middleware');

router.get('/:prescricaoId', controller.show);
router.post('/', auth.hasRole('medico'), controller.create);
router.put('/:prescricaoId', auth.hasRole('medico'), controller.edit);
router.put('/:prescricaoId/aviar', auth.hasRole('farmaceutico'), controller.aviar);

module.exports = router;