const utils   = require('../../components/utils');
const Receita = require('../../models/Receita');
const moment  = require('moment');
const log4js = require('log4js');
log4js.configure({
    appenders: {
      app: { type: 'file', filename: './log/app.log', maxLogSize: 10485760, numBackups: 3 }
    },
    categories: {
      default: { appenders: ['app'], level: 'info' }
    }
  });

const logger = log4js.getLogger('info');

function prescricoes(req, res){
    logger.info("A fazer a mostrar as prescrições para o utente " + req.user);
    let query = {
        utente                   : req.user,
        'prescricoes.quantidade' : {'$gt': 0}
    };
    if(req.query.data){
        query['prescricoes.dataValidade'] = {'$lte': moment(req.query.data).endOf('day').toISOString()};
    }
    Receita.find(query)
    .populate('prescricoes.aviamentos.farmaceutico')
    .sort('prescricoes.dataValidade')
    .then(receitas => {
        let prescricoes = [];
        // Filter the prescriptions
        for(let receita of receitas){
            for(let prescricao of receita.prescricoes){
                let isValid = prescricao.quantidade > 0;
                if(req.query.data){
                    isValid &= moment(prescricao.dataValidade).isSameOrBefore(moment(req.query.data), 'day');
                }

                if(isValid){
                    prescricoes.push(prescricao);
                }
            }
        }
        // Now sort them by date
        prescricoes = prescricoes.sort((a, b) => {
            return moment(a.dataValidade).diff(moment(b.dataValidade));
        });

        return res.status(200).json(prescricoes);
    }).catch(utils.handleError(req, res));
}

module.exports = {
    prescricoes : prescricoes
};