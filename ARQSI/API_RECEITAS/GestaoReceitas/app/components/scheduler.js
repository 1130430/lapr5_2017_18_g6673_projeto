const scheduler = require('node-schedule');
const socket    = require('./websocket');
const mailer    = require('./mailer');
const Receita   = require ('../models/Receita');
const moment    = require('moment');

function begin() {
    // Watch the end of prescriptions once every minute
    scheduler.scheduleJob('*/10 * * * * *', watchExpiringPrescriptions);
}

function watchExpiringPrescriptions() {
    const limit = moment().endOf('day').add('1', 'days');
    Receita.find({
        'prescricoes.notificado': false, 
        'prescricoes.quantidade': {$gt: 0},
        'prescricoes.dataValidade': {$lt: limit}
    })
    .populate('utente')
    .then(receitas => {
        // Filter the prescriptions that need to be notified
        for(let receita of receitas){
            for(let p of receita.prescricoes){
                let date = moment(p.dataValidade);
                if(date.isBefore(limit) && !p.notificado){
                    // Notify the user
                    socket.notify(receita.utente, {
                        text: `Tem uma prescrição a expirar no dia ${date.format('DD/MM/YYYY')}`, 
                        id: receita._id
                    });

                    mailer.sendMail({
                        to: receita.utente.email, 
                        subject: "Alerta: prescrições a expirar", 
                        text: `
                        Ex.ᵐᵒ, Ex.ᵐᵃ ${receita.utente.name}
                        
                        Informamos que tem uma prescrição a expirar no dia ${date.format('DD/MM/YYYY')}.
                        Não se esqueça de visitar uma farmácia para aviar a prescrição de forma a não perder os seus benefícios.
                        
                        Melhores cumprimentos.`
                    });

                    // Update the field so the user doesn't get notified twice
                    p.notificado = true;
                }
            }
            receita.save();
        };
    });
}

module.exports = {
    begin: begin
};