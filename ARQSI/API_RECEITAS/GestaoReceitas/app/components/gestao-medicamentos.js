const axios = require('axios');
const config = require('../config/environment');

function handleErrorResponse(error){
    const originalRequest = error.config;
    
    if (error.response && error.response.status === 401) {
        //Automatically authenticate the app
        return client.post('/api/auth/login/', {
            email    : config.gestaoMedicamentos.email,
            password : config.gestaoMedicamentos.password
        })
        .then(({data}) => {
            //Change the default headers and perform the previous request
            client.defaults.headers.common.Authorization = 'Bearer ' + data.token;
            originalRequest.headers.Authorization = 'Bearer ' + data.token;
            return axios(originalRequest);
        });
    }
    
    //The error is not related to the authentication, proceed with normal behaviour
    return Promise.reject(error);
    
}

function createInstance(){
    return axios.create({
        baseURL: config.gestaoMedicamentos.url
    });
}


let client = createInstance();

client.interceptors.response.use((response) => response, handleErrorResponse);

module.exports = client;