const mongoose    = require('mongoose');
const Schema      = mongoose.Schema;
const idValidator = require('mongoose-id-validator');
const timestamps  = require('../components/timestamps');
const moment      = require('moment');
const gdm         = require('../components/gestao-medicamentos');

let AviamentoSchema = new Schema({
    data : {
        type: Date,
        default: Date.now
    },
    farmaceutico : {
        type : mongoose.Schema.Types.ObjectId,
        ref  : 'User'
    },
    quantidade : {
        type     : Number,
        min      : 1,
        required : [true, 'A quantidade de medicamentos aviados é obrigatória']
    }
});

let PrescricaoSchema = new Schema({
    notificado: {
        type: Boolean,
        default: false
    },
    aviamentos: {
        type: [AviamentoSchema]
    },
    quantidade : {
        type     : Number,
        required : [true, 'Deve definir uma quantidade'],
        min      : 0
    },
    dataValidade : {
        type     : Date,
        required : true,
        validate : {
            validator: function(v){
                let today = moment();
                return moment(v).isSameOrAfter(today, 'day');
            },
            message: 'A validade da prescrição deve ser uma data no futuro'
        }
    },
    idMedicamento : {
        type: Number,
        validate: {
            validator: function(v){
                if(v === 0) return true;
                return gdm.get(`/api/medicamentos/${v}`);
            }
        }
        
    },
    idApresentacao : {
        type: Number,
        required: true,
        validate: {
            validator: function(v){
                return gdm.get(`/api/apresentacoes/${v}`);
            }
        }    
    },
    posologia : {
        type: String,
        required: [true, 'A posologia é obrigatória']
    },
    //The fields bellow are automatically changed before saving the schema
    nomeMedicamento : String,
    forma           : String,
    embalagem       : String,
    concentracao    : String,
    nomeFarmaco     : String,
    idFarmaco       : Number,
});

let ReceitaSchema = new Schema({
    medico: {
        type     : mongoose.Schema.Types.ObjectId,
        ref      : 'User',
        required : true
    },
    utente: {
        type     : mongoose.Schema.Types.ObjectId,
        ref      : 'User',
        required : true
    },
    prescricoes: {
        type     : [PrescricaoSchema],
        validate : {
            validator: function(v){
                return v.length >= 1;
            },
            message : 'A receita deve ter no mínimo uma prescrição'
        }
    }
});

ReceitaSchema.plugin(idValidator);
ReceitaSchema.plugin(timestamps);

ReceitaSchema.methods.aviar = function(prescricaoId, quantidade, farmaceutico){
    return new Promise((resolve, reject) => {
        let prescricao = this.prescricoes.id(prescricaoId);

        if(!prescricao){
            return reject({error: 'not_found', message: 'A prescrição não existe'});
        }

        //Verificar se ainda é possivel aviar medicamentos
        if(moment(prescricao.dataValidade).isBefore(moment(), 'day') || prescricao.quantidade == 0){
            return reject({error: 'validation_error', message: 'Já não é possivel aviar mais medicamentos desta prescrição'});
        }

        //Verificar se a quantidade a aviar não é superior à quantidade existente
        if(prescricao.quantidade - quantidade < 0){
            return reject({error: 'validation_error', message: `Só pode aviar no máximo ${prescricao.quantidade} medicamentos`});
        }


        prescricao.quantidade -= quantidade;
        prescricao.aviamentos.push({
            farmaceutico : farmaceutico,
            quantidade   : quantidade,
        });

        this.save()
        .then((receita) => resolve(prescricao))
        .catch(reject);
    });
};

//////////// Validate and populate prescription fields from gdm api ///////////////
PrescricaoSchema.pre('save', true, function(next, done){
    next(); //Proccess the next middleware
    
    let promises = [];
    ///////// Fetch the medicine ///////////////
    if(this.isModified('idMedicamento')){
        if(this.idMedicamento == 0){ //No medicine
            promises.push({data: {nome: "---"}}); // Simulate an http response
        } else {
            promises.push(gdm.get(`/api/medicamentos/${this.idMedicamento}`));
        }
    } else {
        promises.push({});
    }

    /////////// Fetch the presentation ///////////
    promises.push(this.isModified('idApresentacao') ? gdm.get(`/api/apresentacoes/${this.idApresentacao}`) : {});
    
    let that = this;
    
    Promise.all(promises)
    .then(function(responses){
        let medicamento = responses[0].data;
        let apresentacao = responses[1].data;
        
        if(medicamento){
            that.nomeMedicamento = medicamento.nome;
        }
        
        if(apresentacao){
            that.forma = apresentacao.forma;
            that.embalagem = apresentacao.quantidade;
            that.concentracao = apresentacao.concentracao;
            that.nomeFarmaco = apresentacao.nomeFarmaco;
            that.idFarmaco = apresentacao.farmacoId;
        }
        
        done(); //Finish this middleware
    })
    .catch(err => done(err));
    
    
});

module.exports = mongoose.model('Receita', ReceitaSchema);