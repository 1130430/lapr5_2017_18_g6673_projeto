const _ = require('lodash');

let env = process.env.NODE_ENV || 'development';

// All configurations will extend these options
// ============================================
let all = {
    env: env,
    
    // Server port
    port: process.env.PORT || 9000,
    
    // Mongoose connection 
    mongoose: {
        uri: 'mongodb://lapr5:lapr5@ds249757.mlab.com:49757/gestao_receitas'
        
    },

    jwt: {
        secret   : "fSk35bzq6KutR0dQVKTL",
        issuer   : "http://projeto.arqsi.local",
        audience : "Everyone"
    },

    gestaoMedicamentos: {
        url      : 'https://gestaomedicamentoslapr5.azurewebsites.net',
        email    : '1140367@isep.ipp.pt',
        password : 'VinteEuros15,'
    },

    mail: {
        host     : 'mail.smtp2go.com',
        port     : 2525,
        username : 'lapr5',
        password : 'NnZxbTJqYnU4d3Aw'
    }
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
    all,
    require(`./${env}.js`)
);