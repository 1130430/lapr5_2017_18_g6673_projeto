const Receita = require('../../app/models/Receita');
const User    = require('../../app/models/User');
const assert  = require('chai').assert;
const moment  = require('moment');
const data    = require('../data.json');


describe('Receitas', function(){
    let medico, utente;
    before(() => {
        return User.remove({})
        .then(() => {
            return User.create([data.medico, data.utente]);
        })
        .then(users => {
            data.medico._id = users[0]._id;
            data.utente._id = users[1]._id;
        });
    });
    
    beforeEach(() => {
        return Receita.remove({});
    });
    
    it('should only allow existing medicos', () => {
        let receita = new Receita();
        let validator = receita.validateSync();
        assert.nestedProperty(validator, 'errors.medico', 'The medico field is always required');
        
        receita.medico = "random";
        validator = receita.validateSync();
        assert.nestedProperty(validator ,'errors.medico', 'The medico should be an existing user in the database');
        
        receita.medico = data.medico;
        validator = receita.validateSync();
        assert.notNestedProperty(validator, 'error.medico');
    });
    
    it('should only allow existing utentes', () => {
        let receita = new Receita();
        let validator = receita.validateSync();
        assert.nestedProperty(validator, 'errors.utente', 'The utente field is always required');
        
        receita.utente = "random";
        validator = receita.validateSync();
        assert.nestedProperty(validator ,'errors.utente', 'The utente should be an existing user in the database');
        
        receita.utente = data.utente;
        validator = receita.validateSync();
        assert.notNestedProperty(validator, 'error.utente');
    });
    
    it('should reject receitas without prescricoes', () => {
        let receita = new Receita();
        let validator = receita.validateSync();
        assert.nestedProperty(validator, 'errors.prescricoes');
        
        receita.prescricoes = data.receita.prescricoes;
        validator = receita.validateSync();
        assert.notNestedProperty(validator, 'errors.prescricoes');
    });
    
    it('should reject prescricoes with insufficient prescricoes data', () => {
        let receita = new Receita({
            prescricoes: [{
                
            }]
        });
        let validator = receita.validateSync();
        assert.property(validator, 'errors');
        assert.property(validator.errors, 'prescricoes.0.posologia');
        assert.property(validator.errors, 'prescricoes.0.dataValidade');
        assert.property(validator.errors, 'prescricoes.0.idApresentacao');
        assert.property(validator.errors, 'prescricoes.0.quantidade');
        
        receita.prescricoes[0] = {
            posologia    : '1x por dia',
            idApresentacao : 1,
            dataValidade : moment().add(1, 'days'),
            quantidade: 1
        };
        
        validator = receita.validateSync();
        assert.notProperty(validator.errors, 'prescricoes.0.posologia');
        assert.notProperty(validator.errors, 'prescricoes.0.dataValidade');
        assert.notProperty(validator.errors, 'prescricoes.0.idApresentacao');
        assert.notProperty(validator.errors, 'prescricoes.0.quantidade');
    });
    
    it('should not allow the creation of receitas in the past', () => {
        let receita = new Receita({
            prescricoes: [{
                dataValidade: moment().subtract(1, 'days')
            }]
        });
        
        let validator = receita.validateSync();
        assert.property(validator.errors, 'prescricoes.0.dataValidade');
        
        receita.prescricoes[0].dataValidade = moment();
        validator = receita.validateSync();
        assert.notProperty(validator.errors, 'prescricoes.0.dataValidade');
    });
});