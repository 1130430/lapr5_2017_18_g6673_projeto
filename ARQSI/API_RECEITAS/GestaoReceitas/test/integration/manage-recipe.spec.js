const Receita     = require('../../app/models/Receita');
const User        = require('../../app/models/User');
const chai        = require('chai');
const chaiHttp    = require('chai-http');
const server      = require('../../server');
const expect      = chai.expect;
const moment      = require('moment');
const data        = require('../data.json');
const authService = require('../../app/api/auth/auth.service');

chai.use(chaiHttp);


describe('/api/receitas', function(){
    
    describe('when no token is provided', function(){
        it('cannot list receipts', function(done){
            chai.request(server)
            .get('/api/receitas')
            .send()
            .end(function(err, res){
                expect(err).to.not.be.null;
                
                expect(res).to.have.status(401);
                
                done();
            });
        });
        
        it('cannot create receipts', function(done){
            chai.request(server)
            .post('/api/receitas')
            .send()
            .end(function(err, res){
                expect(err).to.not.be.null;
                
                expect(res).to.have.status(401);
                
                done();
            });
        });
        
        it('cannot show receipts', function(done){
            chai.request(server)
            .get('/api/receitas/randomIdHere')
            .send()
            .end(function(err, res){
                expect(err).to.not.be.null;
                
                expect(res).to.have.status(401);
                
                done();
            });
        });
    });
    
    describe('when token is provided', function(){
        before(function(){
            return User.remove({})
            .then(() => {
                return User.create([data.medico, data.farmaceutico,data.utente]);
            })
            .then(users => {
                data.medico._id       = users[0]._id;
                data.farmaceutico._id = users[1]._id;
                data.utente._id       = users[2]._id;
                
                data.medico.token = authService.signToken(data.medico).token;
                data.farmaceutico.token = authService.signToken(data.farmaceutico).token;
                data.utente.token = authService.signToken(data.utente).token;
            });
        });

        beforeEach(function(){ //Before each test we empty the database
            return Receita.remove({});
        });
        
        describe('POST /', function(){
            it('should not be allowed to pharmaceutists',  function(done){
                chai.request(server)
                .post('/api/receitas')
                .set('Authorization', 'Bearer ' + data.farmaceutico.token)
                .send(data.receita)
                .end(function(err, res){
                    expect(err).to.not.be.null;
                    expect(res).to.have.status(403);
                    done();
                });
            });

            it('should not be allowed to users',  function(done){
                chai.request(server)
                .post('/api/receitas')
                .set('Authorization', 'Bearer ' + data.utente.token)
                .send(data.receita)
                .end(function(err, res){
                    expect(err).to.not.be.null;
                    expect(res).to.have.status(403);
                    done();
                });
            });
            
            it('should be able to create a valid receipt', function(done){
                this.timeout(10000);
                data.receita.medico = data.medico._id;
                data.receita.utente = data.utente._id;
                
                chai.request(server)
                .post('/api/receitas')
                .set('Authorization', 'Bearer ' + data.medico.token)
                .send(data.receita)
                .end(function(err, res){
                    expect(err).to.be.null;
                    expect(res).to.have.status(201);
                    expect(res.body).to.be.a('object');
                    
                    Receita.find({})
                    .then(receitas => {
                        expect(receitas).to.be.an('array').that.has.lengthOf(1);
                        done();
                    });
                });
            });
            
            it('should not allow invalid receipts', function(done){
                chai.request(server)
                .post('/api/receitas')
                .set('Authorization', 'Bearer ' + data.medico.token)
                .send({
                    dataLimite: moment().subtract(1, 'days')
                })
                .end(function(err, res){
                    expect(err).to.not.be.null;
                    expect(res).to.have.status(422);
                    
                    Receita.find({})
                    .then(receitas => {
                        expect(receitas).to.be.an('array').that.has.lengthOf(0);
                        done();
                    });
                });
            });
        });
        
        describe('GET /', function(){
            it('should not allow access to pharmaceutists', function(done){
                chai.request(server)
                .get('/api/receitas')
                .set('Authorization', 'Bearer ' + data.farmaceutico.token)
                .send()
                .end((err, res) => {
                    expect(err).to.not.be.null;
                    expect(res).to.have.status(403);
                    done();
                });
            });
            
            it('should retrieve receipts created by the doctor when requested by a doctor', function(){
                //Given we have 2 receipts, one created by 'medico' and the other created
                //by 'farmaceutico', when we request the server for 'medico's' receipts
                //it should only retrieve receipts created by that 'medico'
                data.receita.medico = data.medico._id;
                data.receita.utente = data.utente._id;

                data.receita2.medico = data.farmaceutico._id;
                data.receita2.utente = data.utente._id;

                return Receita.create([data.receita, data.receita2])
                .then(receitas => {
                    return chai.request(server)
                    .get('/api/receitas')
                    .set('Authorization', 'Bearer ' + data.medico.token);
                })
                .then(res => {
                    expect(res).to.have.status(200);
                    
                    expect(res.body).to.be.an('array');
                    expect(res.body).to.have.length(1);
                    expect(res.body[0]).to.have.nested.property('medico._id', data.medico._id.toString());                    
                });
            });

            it('should retrieve receipts for the user when requestd by a user', function(){
                //Given we have 2 receipts, one created for 'utente' and the other created
                //for 'farmaceutico', when we request the server for 'utente's' receipts
                //it should only retrieve receipts for that 'utente'
                data.receita.medico = data.medico._id;
                data.receita.utente = data.utente._id;

                data.receita2.medico = data.medico._id;
                data.receita2.utente = data.farmaceutico._id;

                return Receita.create([data.receita, data.receita2])
                .then(receitas => {
                    return chai.request(server)
                    .get('/api/receitas')
                    .set('Authorization', 'Bearer ' + data.utente.token);
                })
                .then(res => {
                    expect(res).to.have.status(200);
                    
                    expect(res.body).to.be.an('array');
                    expect(res.body).to.have.length(1);
                    expect(res.body[0]).to.have.nested.property('utente._id', data.utente._id.toString());
                });
            });
        });

        describe('GET /:id', function(){
            it('should not show receipts created by a different doctor', function(done){
                data.receita.medico = data.medico._id;
                data.receita.utente = data.utente._id;

                data.receita2.medico = data.farmaceutico._id;
                data.receita2.utente = data.utente._id;

                Receita.create([data.receita, data.receita2])
                .then(receitas => {
                    chai.request(server)
                    .get(`/api/receitas/${receitas[1]._id}`)
                    .set('Authorization', 'Bearer ' + data.medico.token)
                    .end((err, res) => {
                        expect(err).to.not.be.null;
                        expect(res).to.have.status(404);

                        chai.request(server)
                        .get(`/api/receitas/${receitas[0]._id}`)
                        .set('Authorization', 'Bearer ' + data.medico.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object').with.property('_id', receitas[0]._id.toString());

                            done();
                        });
                    });
                });
            });

            it('should not show receipts of the given user', function(done){
                data.receita.medico = data.medico._id;
                data.receita.utente = data.utente._id;

                data.receita2.medico = data.medico._id;
                data.receita2.utente = data.farmaceutico._id;

                Receita.create([data.receita, data.receita2])
                .then(receitas => {
                    chai.request(server)
                    .get(`/api/receitas/${receitas[1]._id}`)
                    .set('Authorization', 'Bearer ' + data.utente.token)
                    .end((err, res) => {
                        expect(err).to.not.be.null;
                        expect(res).to.have.status(404);

                        chai.request(server)
                        .get(`/api/receitas/${receitas[0]._id}`)
                        .set('Authorization', 'Bearer ' + data.utente.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            expect(res).to.have.status(200);
                            expect(res.body).to.be.an('object').with.property('_id', receitas[0]._id.toString());

                            done();
                        });
                    });
                });
            });
        });
    });
});

