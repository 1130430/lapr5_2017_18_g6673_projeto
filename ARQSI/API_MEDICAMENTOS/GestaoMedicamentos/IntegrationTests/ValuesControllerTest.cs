using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using GestaoMedicamentos;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests
{
    
    public class ValuesControllerTest : IClassFixture<TestFixture<Startup>>
    {
        private readonly HttpClient _client;

        public ValuesControllerTest(TestFixture<Startup> fixture)
        {
            _client = fixture.Client;
        }

        [Fact]
        public async Task SimpleTest()
        {
            // Act
            var response = await _client.GetAsync("/api/values");

            // Assert
            response.EnsureSuccessStatusCode();
        }
    }
}
