﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests
{
    public class MedicamentosControllerTest : IClassFixture<TestFixture<Startup>>
    {
        internal class MedicamentoRequestDTO
        {
            public MedicamentoRequestDTO(string name, int apresentacaoId)
            {
                Name = name;
                ApresentacaoId = apresentacaoId;
            }

            public string Name { get; set; }
            public int ApresentacaoId { get; set; }
        }

        private readonly HttpClient _client;

        public MedicamentosControllerTest(TestFixture<Startup> fixture)
        {
            _client = fixture.Client;
        }

        [Fact]
        public async Task TestRoutesAreProtected()
        {
            // Act
            var response = await _client.GetAsync("/api/medicamentos");

            // Assert
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Fact]
        public async Task AssertMedicamentosCannotBeInvalid()
        {
            //Arrange
            MedicamentoRequestDTO med = new MedicamentoRequestDTO("", 0);
            
            // Act
            var response = await _client.PostAsJsonAsync("/api/medicamentos", med);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        internal class LoginDTO
        {
            public string Email { get; set; }
            public string Password { get; set; }
        }
    }
}
