﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using GestaoMedicamentos.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using GestaoMedicamentos.Data;

namespace IntegrationTests
{
    public class Startup
    {

        public IConfiguration Configuration { get; }
        public Startup()
        {
            Configuration = TestFixture<Startup>.GetConfiguration();
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ProjetoContext>(
                optionsBuilder => optionsBuilder.UseInMemoryDatabase("InMemoryDb"));

            services.AddMvc();

            services.AddIdentity<UserEntity, IdentityRole>()
                .AddEntityFrameworkStores<ProjetoContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "JwtBearer";
                options.DefaultChallengeScheme = "JwtBearer";
            })
            .AddJwtBearer("JwtBearer", jwtBearerOptions =>
            {
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                        Configuration.GetSection("Jwt:Secret").Value
                    )),

                    ValidateIssuer = true,
                    ValidIssuer = Configuration.GetSection("Jwt:Issuer").Value,

                    ValidateAudience = true,
                    ValidAudience = Configuration.GetSection("Jwt:Audience").Value,

                    ValidateLifetime = true, //validate the expiration and not before values in the token

                    ClockSkew = TimeSpan.FromMinutes(5) //5 minute tolerance for the expiration date
                };
            });

            services.AddMvc()
                .AddJsonOptions(options => options.SerializerSettings.DateFormatString = "dd/MM/yyyy");
            
        }

        public void Configure(IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory)
        {
            var context = app.ApplicationServices.GetService<ProjetoContext>();
            var userManager = app.ApplicationServices.GetService<UserManager<UserEntity>>();
            DbInitializer.Initialize(context, userManager);
            app.UseMvc();
        }
    }
}