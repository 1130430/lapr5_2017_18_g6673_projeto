﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using GestaoMedicamentos.Models;
using GestaoMedicamentos.Data;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;

namespace GestaoMedicamentos
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var userManager = services.GetRequiredService<UserManager<UserEntity>>();
                var context = services.GetRequiredService<ProjetoContext>();
                DbInitializer.Initialize(context, userManager);
            }

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
