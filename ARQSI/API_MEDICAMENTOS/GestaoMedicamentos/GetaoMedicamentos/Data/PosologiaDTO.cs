﻿using GestaoMedicamentos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoMedicamentos.Data
{
    public class PosologiaDTO
    {
        public PosologiaDTO(Posologia model)
        {
            Id = model.Id;
            FaixaEtaria = model.FaixaEtaria;
            Frequencia = model.Frequencia;
            ApresentacaoId = model.ApresentacaoId;
        }
        public int Id { get; set; }
        public string FaixaEtaria { get; set; }
        public string Frequencia { get; set; }
        public int ApresentacaoId { get; set; }
    }
}
