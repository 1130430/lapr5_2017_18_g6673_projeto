﻿using GestaoMedicamentos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoMedicamentos.Data
{
    public class MedicamentoDTO
    {
        public MedicamentoDTO(Medicamento medicamento)
        {
            ID = medicamento.Id;
            Nome = medicamento.Nome;
            ApresentacaoId = medicamento.ApresentacaoId;
        }
        public int ID { get; set; }
        public string Nome { get; set; }
        public int ApresentacaoId { get; set; }
    }
}
