﻿using GestaoMedicamentos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoMedicamentos.Data
{
    public class ApresentacaoDTO
    {
        public ApresentacaoDTO(Apresentacao model)
        {
            Id = model.Id;
            Forma = model.Forma;
            Concentracao = model.Concentracao;
            Quantidade = model.Quantidade;
            FarmacoId = model.FarmacoId;
        }
        public int Id { get; set; }
        public string Forma { get; set; }
        public string Concentracao { get; set; }
        public string Quantidade { get; set; }
        public int FarmacoId { get; set; }
    }
}
