﻿using GestaoMedicamentos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoMedicamentos.Data
{
    public class FarmacoDTO
    {
        public FarmacoDTO(Farmaco farmaco)
        {
            Id = farmaco.Id;
            Nome = farmaco.Nome;
        }

        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
