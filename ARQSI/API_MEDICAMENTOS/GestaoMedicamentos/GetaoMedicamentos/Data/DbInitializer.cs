﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using GestaoMedicamentos.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoMedicamentos.Data
{
    public static class DbInitializer
    {

        public static void Initialize(ProjetoContext context, UserManager<UserEntity> userManager)
        {

            context.Database.EnsureCreated();
            if (context.Users.Any())
            {
                return; //DB has been seeded
            }

            //CreateUsers(context, userManager);

            CreateFarmacos(context);

            CreateApresentacoes(context);

            CreateMedicamentos(context);

            CreatePosologias(context);
        }

        public static void CreatePosologias(ProjetoContext context)
        {
            var posologias = new Posologia[]
            {
                new Posologia{ ApresentacaoId = 1, FaixaEtaria = "Geral", Frequencia = "1x/dia"},
                new Posologia{ ApresentacaoId = 1, FaixaEtaria = "1-10", Frequencia = "2x/dia"},
                new Posologia{ ApresentacaoId = 2, FaixaEtaria = "Geral", Frequencia = "2x/dia"},
                new Posologia{ ApresentacaoId = 2, FaixaEtaria = "Idosos", Frequencia = "3x/dia"},
                new Posologia{ ApresentacaoId = 3, FaixaEtaria = "Adultos", Frequencia = "12 em 12 horas"},
                new Posologia{ ApresentacaoId = 3, FaixaEtaria = "Crianças", Frequencia = "8 em 8 horas"},
                new Posologia{ ApresentacaoId = 3, FaixaEtaria = "Idosos", Frequencia = "10 em 10 horas"},
                new Posologia{ ApresentacaoId = 4, FaixaEtaria = "Crianças", Frequencia = "1x/dia"},
                new Posologia{ ApresentacaoId = 5, FaixaEtaria = "Adolescentes", Frequencia = "4x/dia"},
                new Posologia{ ApresentacaoId = 5, FaixaEtaria = "Idosos", Frequencia = "3x/dia"},
                new Posologia{ ApresentacaoId = 5, FaixaEtaria = "Geral", Frequencia = "1x/dia"},
            };

            foreach (Posologia p in posologias)
            {
                context.Posologia.Add(p);
            }
            context.SaveChanges();
        }

        public static void CreateMedicamentos(ProjetoContext context)
        {
            var medicamentos = new Medicamento[]
            {
                new Medicamento{ ApresentacaoId = 1, Nome = "Brufen 600"},
                new Medicamento{ ApresentacaoId = 2, Nome = "Brufen 400"},
                new Medicamento{ ApresentacaoId = 3, Nome = "Ben-U-Ron Comprimido" },
                new Medicamento{ ApresentacaoId = 3, Nome = "Cêgripe" },
                new Medicamento{ ApresentacaoId = 4, Nome = "Ben-U-Ron Supositório" },
                new Medicamento{ ApresentacaoId = 5, Nome = "Bêcê" },
                new Medicamento{ ApresentacaoId = 5, Nome = "Bêcomvite" }
            };

            foreach (Medicamento m in medicamentos)
            {
                context.Medicamento.Add(m);
            }
            context.SaveChanges();
        }

        public static void CreateApresentacoes(ProjetoContext context)
        {
            var apresentacoes = new Apresentacao[]
            {
                new Apresentacao{ FarmacoId = 1, Concentracao = "600 mg", Forma = "Comprimido", Quantidade = "60 unidades"},
                new Apresentacao{ FarmacoId = 1, Concentracao = "400 mg", Forma = "Comprimido", Quantidade = "60 unidades"},
                new Apresentacao{ FarmacoId = 2, Concentracao = "500 mg", Forma = "Comprimido", Quantidade = "10 unidades"},
                new Apresentacao{ FarmacoId = 2, Concentracao = "1000 mg", Forma = "Supositório", Quantidade = "10 unidades"},
                new Apresentacao{ FarmacoId = 3, Concentracao = "20 mg", Forma = "Comprimido", Quantidade = "10 unidades"},
            };

            foreach (Apresentacao a in apresentacoes)
            {
                context.Apresentacao.Add(a);
            }
            context.SaveChanges();
        }

        public static void CreateFarmacos(ProjetoContext context)
        {
            var farmacos = new Farmaco[]
            {
                new Farmaco { Nome = "Ibuprofeno"},
                new Farmaco { Nome = "Paracetamol"},
                new Farmaco { Nome = "Vitamina B"}
            };

            foreach (Farmaco f in farmacos)
            {
                context.Farmaco.Add(f);
            }
            context.SaveChanges();
        }

        public static void CreateUsers(ProjetoContext context, UserManager<UserEntity> userManager)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            string password = "Password1_";
            var passwordHasher = new PasswordHasher<UserEntity>();
            var users = new UserEntity[]
            {
                new UserEntity{UserName = "1140234@isep.ipp.pt", Name = "Luis Teixeira", Email = "1140234@isep.ipp.pt", Birthday = DateTime.ParseExact("13/01/1996","dd/MM/yyyy", provider)},
                new UserEntity{UserName = "1140367@isep.ipp.pt", Name = "Vitor Rocha", Email = "114367@isep.ipp.pt", Birthday = DateTime.ParseExact("18/05/1996","dd/MM/yyyy", provider)}
            };

            foreach (UserEntity user in users)
            {
                userManager.CreateAsync(user, password);
            }
        }
    }
}
