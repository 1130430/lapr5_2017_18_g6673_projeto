﻿using GestaoMedicamentos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoMedicamentos.Data
{
    public class UserRegisteredDTO
    {
        public UserRegisteredDTO(UserEntity user)
        {
            Email = user.Email;
            Name = user.Name;
            Birthday = user.Birthday.ToShortDateString();
        }

        public string Email { get; set; }
        public string Name { get; set; }
        public string Birthday { get; set; }
    }
}
