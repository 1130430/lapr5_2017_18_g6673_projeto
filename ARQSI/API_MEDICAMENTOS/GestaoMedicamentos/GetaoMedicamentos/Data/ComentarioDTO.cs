﻿using GestaoMedicamentos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoMedicamentos.Data
{
    public class ComentarioDTO
    {
        public ComentarioDTO(Comentario comentario)
        {
            this.Id = comentario.Id;
            this.Texto = comentario.Texto;
            this.ApresentacaoId = comentario.ApresentacaoId;
            this.Data = comentario.Data;
        }

        public int Id { get; set; }

        public string Texto { get; set; }

        public DateTime Data { get; set; }

        public int ApresentacaoId { get; set; }
    }
}
