﻿using GestaoMedicamentos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoMedicamentos.Data
{
    public class MedicamentoCompletoDTO
    {
        public MedicamentoCompletoDTO(Medicamento medicamento)
        {
            ID = medicamento.Id;
            Nome = medicamento.Nome;
            Forma = medicamento.Apresentacao.Forma;
            Concentracao = medicamento.Apresentacao.Concentracao;
            Quantidade = medicamento.Apresentacao.Quantidade;
            ApresentacaoId = medicamento.ApresentacaoId;
            FarmacoId = medicamento.Apresentacao.FarmacoId;
            NomeFarmaco = medicamento.Apresentacao.Farmaco.Nome;
        }

        public int ID { get; set; }
        public string Nome { get; set; }
        public string Forma { get; set; }
        public string Concentracao { get; set; }
        public string Quantidade { get; set; }
        public int ApresentacaoId { get; set; }
        public int FarmacoId { get; set; }
        public string NomeFarmaco { get; set; }
    }
}
