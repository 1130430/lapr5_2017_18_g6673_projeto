﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GestaoMedicamentos.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace GestaoMedicamentos.Models
{
    public class ProjetoContext : IdentityDbContext<UserEntity>
    {
        public ProjetoContext(DbContextOptions<ProjetoContext> options)
            : base(options)
        {
        }

        public DbSet<GestaoMedicamentos.Models.Medicamento> Medicamento { get; set; }

        public DbSet<GestaoMedicamentos.Models.Apresentacao> Apresentacao { get; set; }

        public DbSet<GestaoMedicamentos.Models.Farmaco> Farmaco { get; set; }

        public DbSet<GestaoMedicamentos.Models.Posologia> Posologia { get; set; }

        public DbSet<GestaoMedicamentos.Models.Comentario> Comentario { get; set; }
    }
}
