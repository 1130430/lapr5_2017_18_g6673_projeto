﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoMedicamentos.Data
{
    public class JwtTokenDTO
    {
        public JwtTokenDTO(JwtSecurityToken original)
        {
            Token = new JwtSecurityTokenHandler().WriteToken(original);
            ExpirationDate = original.ValidTo.ToString("dd/MM/yyyy HH:mm:ss");
        }
        public string Token { get; set; }
        public string ExpirationDate { get; set; }
    }
}
