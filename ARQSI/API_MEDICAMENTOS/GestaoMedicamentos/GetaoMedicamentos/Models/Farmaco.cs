﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GestaoMedicamentos.Models
{
    public class Farmaco
    {
        public int Id { get; set; }

        [Required]
        public string Nome { get; set; }
        
    }
}