﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoMedicamentos.Models
{
    public class UserEntity : IdentityUser
    {
        public string Name { get; set; }

        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }
    }
}
