﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoMedicamentos.Models
{
    public class Apresentacao
    {
        public int Id { get; set; }

        [Required]
        public string Forma { get; set; }
        [Required]
        public string Concentracao { get; set; }

        [Required]
        public string Quantidade { get; set; }

        [Required]
        public int FarmacoId { get; set; }

        public Farmaco Farmaco { get; set; }
    }
}
