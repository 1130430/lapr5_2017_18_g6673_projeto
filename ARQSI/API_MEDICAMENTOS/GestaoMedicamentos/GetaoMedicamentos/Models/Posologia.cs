﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoMedicamentos.Models
{
    public class Posologia
    {
        public int Id { get; set; }

        [Required]
        public string FaixaEtaria { get; set; }

        [Required]
        public string Frequencia { get; set; }

        [Required]
        public int ApresentacaoId { get; set; }

        public Apresentacao Apresentacao { get; set; }
    }
}
