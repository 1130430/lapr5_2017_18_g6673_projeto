﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GestaoMedicamentos.Models
{
    public class Medicamento
    {
        public int Id { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public int ApresentacaoId { get; set; }

        public Apresentacao Apresentacao { get; set; }

    }
}
