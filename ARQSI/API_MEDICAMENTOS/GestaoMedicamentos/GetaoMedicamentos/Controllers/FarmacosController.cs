﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GestaoMedicamentos.Models;
using GestaoMedicamentos.Data;
using Microsoft.AspNetCore.Authorization;

namespace GestaoMedicamentos.Controllers
{
    [Produces("application/json")]
    [Authorize]
    [Route("api/Farmacos")]
    public class FarmacosController : Controller
    {
        private readonly ProjetoContext _context;

        public FarmacosController(ProjetoContext context)
        {
            _context = context;
        }

        // GET: api/Farmacos
        [HttpGet]
        public IEnumerable<FarmacoDTO> GetFarmaco(string nome)
        {
            IQueryable<Farmaco> query = _context.Farmaco;
            if (!String.IsNullOrEmpty(nome))
            {
                query = query.Where(f => f.Nome.Equals(nome));
            }

            List<FarmacoDTO> farmacos = query.Select(f => new FarmacoDTO(f)).ToList();
            return farmacos;
        }

        // GET: api/Farmacos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.Id == id);

            if (farmaco == null)
            {
                return NotFound();
            }

            return Ok(new FarmacoDTO(farmaco));
        }

        // PUT: api/Farmacos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFarmaco([FromRoute] int id, [FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != farmaco.Id)
            {
                return BadRequest();
            }

            if (!FarmacoExists(id))
            {
                return NotFound();
            }

            _context.Entry(farmaco).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return Ok(new FarmacoDTO(farmaco));
        }

        // POST: api/Farmacos
        [HttpPost]
        public async Task<IActionResult> PostFarmaco([FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Farmaco.Add(farmaco);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFarmaco", new { id = farmaco.Id }, new FarmacoDTO(farmaco));
        }

        // DELETE: api/Farmacos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.Id == id);
            if (farmaco == null)
            {
                return NotFound();
            }

            _context.Farmaco.Remove(farmaco);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // GET: api/Farmacos/5/medicamentos
        [HttpGet("{id}/medicamentos")]
        public async Task<IActionResult> GetMedicamentos([FromRoute] int id)
        {
            if(!FarmacoExists(id))
            {
                return NotFound();
            }

            return Ok(_context
                .Medicamento
                .Include(m => m.Apresentacao.Farmaco)
                .Where(m => m.Apresentacao.FarmacoId == id)
                .Select(m => new MedicamentoCompletoDTO(m))
           );

        }

        // GET: api/Farmacos/5/apresentacoes
        [HttpGet("{id}/apresentacoes")]
        public async Task<IActionResult> GetApresentacoes([FromRoute] int id)
        {
            if (!FarmacoExists(id))
            {
                return NotFound();
            }

            return Ok(_context
                .Apresentacao
                .Where(a => a.FarmacoId == id)
                .Select(a => new ApresentacaoDTO(a))
           );

        }


        // GET: api/Farmacos/5/posologias
        [HttpGet("{id}/posologias")]
        public async Task<IActionResult> GetPosologias([FromRoute] int id)
        {
            if (!FarmacoExists(id))
            {
                return NotFound();
            }

            int[] apresentacoes = _context.Apresentacao
                .Where(a => a.FarmacoId == id)
                .Select(a => a.Id)
                .ToArray();

            IQueryable<PosologiaDTO> posologias = _context.Posologia
                .Where(p => apresentacoes.Contains(p.ApresentacaoId))
                .Select(p => new PosologiaDTO(p));

            return Ok(posologias);

        }

        private bool FarmacoExists(int id)
        {
            return _context.Farmaco.Any(e => e.Id == id);
        }
    }
}