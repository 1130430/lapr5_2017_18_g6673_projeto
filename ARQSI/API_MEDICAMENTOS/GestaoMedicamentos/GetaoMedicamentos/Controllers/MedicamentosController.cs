﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GestaoMedicamentos.Models;
using GestaoMedicamentos.Data;
using Microsoft.AspNetCore.Authorization;

namespace GestaoMedicamentos.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Medicamentos")]
    public class MedicamentosController : Controller
    {
        private readonly ProjetoContext _context;

        public MedicamentosController(ProjetoContext context)
        {
            _context = context;
        }

        // GET: api/Medicamentos
        [HttpGet]
        public IEnumerable<MedicamentoCompletoDTO> GetMedicamento(string nome)
        {
            IQueryable<Medicamento> query = _context.Medicamento.Include(m => m.Apresentacao.Farmaco);

            if (!String.IsNullOrEmpty(nome))
            {
                query = query.Where(medicamento => medicamento.Nome.Equals(nome));
            }

            List<MedicamentoCompletoDTO> medicamentos = query.Select(m => new MedicamentoCompletoDTO(m)).ToList();

            return medicamentos;
        }

        // GET: api/Medicamentos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamento = await _context.Medicamento.Include(m => m.Apresentacao.Farmaco).SingleOrDefaultAsync(m => m.Id == id);

            if (medicamento == null)
            {
                return NotFound();
            }

            return Ok(new MedicamentoCompletoDTO(medicamento));
        }

        // PUT: api/Medicamentos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicamento([FromRoute] int id, [FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medicamento.Id)
            {
                return BadRequest();
            }

            _context.Entry(medicamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(new MedicamentoDTO(medicamento));
        }

        // POST: api/Medicamentos
        [HttpPost]
        public async Task<IActionResult> PostMedicamento([FromBody] Medicamento medicamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Medicamento.Add(medicamento);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMedicamento", new { id = medicamento.Id }, new MedicamentoDTO(medicamento));
        }

        // DELETE: api/Medicamentos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMedicamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamento = await _context.Medicamento.SingleOrDefaultAsync(m => m.Id == id);
            if (medicamento == null)
            {
                return NotFound();
            }

            _context.Medicamento.Remove(medicamento);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // GET: api/Medicamentos/5/apresentacoes
        [HttpGet("{id}/Apresentacoes")]
        public async Task<IActionResult> GetApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!MedicamentoExists(id))
            {
                return NotFound();
            }

            IQueryable<ApresentacaoDTO> apresentacoes = _context.Medicamento
                .Include(m => m.Apresentacao)
                .Select(m => new ApresentacaoDTO(m.Apresentacao));

            return Ok(apresentacoes);
        }

        // GET: api/Medicamentos/5/posologias
        [HttpGet("{id}/Posologias")]
        public async Task<IActionResult> GetPosologia([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!MedicamentoExists(id))
            {
                return NotFound();
            }

            int ApresentacaoId = _context.Medicamento
                .Include(m => m.Apresentacao)
                .SingleOrDefault(m => m.Id == id)
                .ApresentacaoId;

            IQueryable<PosologiaDTO> posologias = _context.Posologia
                .Where(p => p.ApresentacaoId == ApresentacaoId)
                .Select(p => new PosologiaDTO(p));

            return Ok(posologias);
        }

        private bool MedicamentoExists(int id)
        {
            return _context.Medicamento.Any(e => e.Id == id);
        }
    }
}