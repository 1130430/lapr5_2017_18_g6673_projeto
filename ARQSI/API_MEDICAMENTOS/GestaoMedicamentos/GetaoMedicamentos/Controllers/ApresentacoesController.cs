﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GestaoMedicamentos.Models;
using System.Data.SqlClient;
using GestaoMedicamentos.Data;
using Microsoft.AspNetCore.Authorization;

namespace GestaoMedicamentos.Controllers
{
    [Produces("application/json")]
    [Authorize]
    [Route("api/Apresentacoes")]
    public class ApresentacoesController : Controller
    {
        private readonly ProjetoContext _context;

        public ApresentacoesController(ProjetoContext context)
        {
            _context = context;
        }

        // GET: api/Apresentacoes
        [HttpGet]
        public IEnumerable<ApresentacaoCompletaDTO> GetApresentacao()
        {
            IQueryable<Apresentacao> query = _context.Apresentacao.Include(a => a.Farmaco);

            List<ApresentacaoCompletaDTO> apresentacoes = query.Select(a => new ApresentacaoCompletaDTO(a)).ToList();

            return apresentacoes;
        }

        // GET: api/Apresentacoes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacao = await _context.Apresentacao.Include(a => a.Farmaco).SingleOrDefaultAsync(m => m.Id == id);

            if (apresentacao == null)
            {
                return NotFound();
            }

            return Ok(new ApresentacaoCompletaDTO(apresentacao));
        }

        // PUT: api/Apresentacoes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutApresentacao([FromRoute] int id, [FromBody] Apresentacao apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apresentacao.Id)
            {
                return BadRequest();
            }

            _context.Entry(apresentacao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return Ok(new ApresentacaoDTO(apresentacao));
        }

        // POST: api/Apresentacoes
        [HttpPost]
        public async Task<IActionResult> PostApresentacao([FromBody] Apresentacao apresentacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Apresentacao.Add(apresentacao);
            await _context.SaveChangesAsync();


            return CreatedAtAction("GetApresentacao", new { id = apresentacao.Id }, new ApresentacaoDTO(apresentacao));
        }

        // DELETE: api/Apresentacoes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteApresentacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apresentacao = await _context.Apresentacao.SingleOrDefaultAsync(m => m.Id == id);
            if (apresentacao == null)
            {
                return NotFound();
            }

            _context.Apresentacao.Remove(apresentacao);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // GET: api/Apresentacoes/5/Comentarios
        [HttpGet("{id}/Comentarios")]
        public async Task<IActionResult> GetComentarios([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!ApresentacaoExists(id))
            {
                return NotFound();
            }

            var comentarios = _context.Comentario.Where(c => c.ApresentacaoId == id).Select(c => new ComentarioDTO(c)).ToList();

            return Ok(comentarios);
        }

        // POST: api/Apresentacoes/5/Comentarios
        [HttpPost("{id}/Comentarios")]
        public async Task<IActionResult> PostComentario([FromRoute] int id, [FromBody] Comentario comentario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!ApresentacaoExists(id))
            {
                return NotFound();
            }


            _context.Comentario.Add(comentario);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetComentarios", new { id = comentario.ApresentacaoId }, new ComentarioDTO(comentario));
        }

        // DELETE: api/Apresentacoes/5/Comentarios
        [HttpDelete("{id}/Comentarios")]
        public async Task<IActionResult> DeleteComentarios([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!ApresentacaoExists(id))
            {
                return NotFound();
            }

            var comentarios = _context.Comentario.Where(c => c.ApresentacaoId == id).ToList();
            foreach(Comentario c in comentarios)
            {
                _context.Comentario.Remove(c);
            }
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ApresentacaoExists(int id)
        {
            return _context.Apresentacao.Any(e => e.Id == id);
        }
    }
}